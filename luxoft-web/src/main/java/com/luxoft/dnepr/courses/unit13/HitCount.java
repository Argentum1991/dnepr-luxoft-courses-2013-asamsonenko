package com.luxoft.dnepr.courses.unit13;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class HitCount extends HttpServlet {
    /*
    HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Content-Length:  (total  length)
{"hitCount": 10}
     */
    volatile static int numberOfHit = 0;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setContentType("application/json");
        response.setStatus(200, "Ok");
        response.addHeader("charset", "utf-8");
        PrintWriter out = response.getWriter();
        numberOfHit++;
        String dataResponse = String.format("\"hitCount\": %d", numberOfHit);
        out.println(dataResponse);
        response.setContentLength(dataResponse.length());
    }
}

