package com.luxoft.dnepr.courses.unit13;

import java.io.IOException;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.*;
import javax.servlet.http.*;


public class Dummy extends HttpServlet {
    static Lock lockForMap = new ReentrantLock();
    static Map<String, String> dataMap = new HashMap<>();
    /*
 HTTP/1.1 500 Internal Server Error
 Content-Type: application/json; charset=utf-8
 Content-Length: (total length)
 {"error": "Illegal parameters"}

 Если name уже был сохранен, возвращаем
 HTTP/1.1 500 Internal Server Error
 Content-Type: application/json; charset=utf-8
 Content-Length: (total length)
 {"error": "Name ${name} already exists"}

 Если name еще не был сохранен, то сохраняем пару name/age и возвращаем
 HTTP/1.1 201 Created
  */
    public void doPut(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        PrintWriter out = response.getWriter();
        response.addHeader("charset", "utf-8");
        String nameReq = request.getParameter("name");
        String ageReq = request.getParameter("age");
        String servResponse = "";
        if ((nameReq == null) || (ageReq == null)) {
            response.setStatus( 500,"Internal Server Error");
            servResponse = String.format("\"error\": \"Illegal parameters");
            out.println(servResponse);
            response.setContentLength(servResponse.length());
        } else {
            try {
                lockForMap.lock();
                if (dataMap.containsKey(nameReq)) {
                    response.setStatus(500,"Internal Server Error");
                    //{"error": "Name ${name} already exists"}
                    out.println(servResponse);
                    response.setContentLength(servResponse.length());
                    servResponse = String.format("\"error\": \"%s already exists\"", nameReq);
                    out.println(servResponse);
                    response.setContentLength(servResponse.length());
                } else {
                    dataMap.put(nameReq,ageReq);
                    response.setStatus(201, "Created");
                }
            } finally {
                lockForMap.unlock();
            }

        }


    }
    /*
    Метод POST принимает 2 параметра name и age.
Если один из них отсутствует, возвращаем
HTTP/1.1 500 Internal Server Error
Content-Type: application/json; charset=utf-8
Content-Length: (total length)
{"error": "Illegal parameters"}

Если name не был сохранен, возвращаем
HTTP/1.1 500 Internal Server Error
Content-Type: application/json; charset=utf-8
Content-Length: (total length)
{"error": "Name ${name} does not exist"}

Если name был сохранен, то обновляем его age и возвращаем
HTTP/1.1 202 Accepted
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        PrintWriter out = response.getWriter();
        response.addHeader("charset", "utf-8");
        String nameReq = request.getParameter("name");
        String ageReq = request.getParameter("age");
        String servResponse = "";
        out.println("WORKING" +nameReq+ageReq);

        if ((nameReq == null) || (ageReq == null)) {
            response.setStatus(500,"Internal Server Error");
            servResponse = String.format("\"error\": \"Illegal parameters");
            out.println(servResponse);
            response.setContentLength(servResponse.length());
        } else {
            try {
                lockForMap.lock();
                if (!dataMap.containsKey(nameReq)) {
                    response.setStatus(500,"Internal Server Error");
                    //{"error": "Name ${name} does not exists"}
                    out.println(servResponse);
                    response.setContentLength(servResponse.length());
                    servResponse = String.format("\"error\": \"%s does not exists\"", nameReq);
                    out.println(servResponse);
                    response.setContentLength(servResponse.length());
                } else {
                    dataMap.put(nameReq,ageReq);
                    response.setStatus(202, "Accepted");
                }
            } finally {
                lockForMap.unlock();
            }

        }


    }

}
