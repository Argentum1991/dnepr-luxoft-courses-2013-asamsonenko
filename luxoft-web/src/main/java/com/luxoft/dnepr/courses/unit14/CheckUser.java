package com.luxoft.dnepr.courses.unit14;


import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


public class CheckUser extends HttpServlet {
    ConcurrentMap<String, String> passLogin;

    public void init()
            throws ServletException {
        passLogin = new ConcurrentHashMap<>();
        String content = getServletContext().getInitParameter("users");
        String[] initUsers = content.substring(content.indexOf("{") + 1, content.indexOf("}")).split(",");
        for (String entry : Arrays.asList(initUsers)) {
            String[] pairs = entry.split(":");
            passLogin.put(pairs[0].substring(pairs[0].indexOf("\"") + 1, pairs[0].lastIndexOf("\"")),
                    pairs[1].substring(pairs[1].indexOf("\"") + 1, pairs[1].lastIndexOf("\"")));
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
    doRequest(request,response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    doRequest(request,response);
    }
    public void doRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (checkRequest(request)) {
            HttpSession session = request.getSession(true);
            synchronized (session){
            session.setAttribute("login", request.getParameter("login"));
            }
                forwardTo("/user.jsp", request, response);
            return;
        }
        request.setAttribute("status", "invalid");
        forwardTo("/index.jsp", request, response);
    }

    public void forwardTo(String destination, HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            RequestDispatcher toDestination = request.getRequestDispatcher(destination);
            toDestination.forward(request, response);
        } catch (ServletException ex) {
            ex.printStackTrace();
        }

    }

    public boolean checkRequest(HttpServletRequest request) {
        String login = request.getParameter("login");
        String pass = request.getParameter("password");
        if (login != null && pass != null) {
            if (passLogin.containsKey(login)) {
                if (passLogin.get(login).equals(pass)) {
                    return true;
                }
            }
        }
        return false;
    }
}
