
/*query1*/
SELECT 
    model, speed, hd
FROM
    pc
WHERE
    (cd = '12x' OR cd = '24x')
        AND (price < 600);

/*query2*/
SELECT 
    speed, maker_name
FROM
    laptop
        INNER JOIN
    product USING (model)
        INNER JOIN
    makers USING (maker_id)
WHERE
    (hd >= 10);


/*query3*/

SELECT 
    model, speed, hd
FROM
    pc
WHERE
    (price < 500);

/*query4*/
SELECT DISTINCT
    maker_name
FROM
    product
        INNER JOIN
    makers USING (maker_id)
WHERE
    type = 'Printer'
ORDER by maker_name DESC;

/*query5*/
SELECT 
    @makerId:=maker_id
FROM
    makers
WHERE
    maker_name = 'B';

SELECT 
    model, price
FROM
    pc
WHERE
    model in (SELECT 
            model
        FROM
            product
        WHERE
            maker_id = @makerId) 
UNION SELECT 
    model, price
FROM
    laptop
WHERE
    model in (SELECT 
            model
        FROM
            product
        WHERE
            maker_id = @makerId) 
UNION SELECT 
    model, price
FROM
    printer
WHERE
    model in (SELECT 
            model
        FROM
            product
        WHERE
            maker_id = @makerId);



/*query6*/
CREATE TABLE IF NOT EXISTS tmpId SELECT DISTINCT p1.maker_id From
    product p1,
    product p2
Where
    p1.maker_id = p2.maker_id
        and 'pc' = any (select 
            type
        from
            product
        where
            maker_id = p2.maker_id)
        and 'laptop' != all (select 
            type
        from
            product
        where
            maker_id = p2.maker_id);

Select 
    maker_name
From
    makers
        INNER JOIN
    tmpId ON makers.maker_id = tmpId.maker_id;
drop table tmpId;



/*query7*/
Select 
    maker_name
From
    makers
Where
    makers.maker_id in (Select DISTINCT
            product.maker_id
        from
            product
                Inner Join
            pc USING (model)
        Where
            pc.speed >= 450);



/*query8*/
Select 
    @maxPrice:=max(price)
From
    printer;
SELECT 
    model, price
FROM
    printer
Where
    price = @maxPrice;

/*query9*/
Select 
    AVG(speed)
From
    laptop
Where
    price > 1000;


/*query10*/
Select 
    AVG(speed)
From
    pc
        Inner Join
    product USING (model)
        Inner Join
    makers USING (maker_id)
where
    makers.maker_name = 'a'; 


/*query11*/
drop table if exists tmpUniqType;
drop table if exists tmpCountModel;

Create table tmpUniqType Select DISTINCt p1.maker_id, p1.type from
    product as p1,
    product as p2
Where
    p1.maker_id = p2.maker_id
        and p1.type = all (select 
            type
        from
            product
        where
            product.maker_id = p1.maker_id);

create table tmpCountModel select maker_id, count(maker_id) from
    makers
        inner join
    product USING (maker_id)
group by maker_name
having count(maker_id) > 1;

Select maker_id, type From tmpUniqType inner join tmpCountModel
using (maker_id);


/*query12*/
select hd from 
(select hd, count(hd) from pc group by hd having count(hd)>=2) as res
order by hd desc;

/*query13*/
select type,tmp.model,tmp.speed from product 
inner join (select model,speed from laptop where speed< all 
(select speed from pc)) as tmp using (model);

/*query14*/
select 
    @minPrice:=min(price)
from
    printer
where
    color = 'y';

select 
    maker_name, price
from
    makers
        inner join
    (select 
        maker_id, price
    from
        product
    inner join printer USING (model)
    where
        printer.price = @minPrice
            and color = 'y') as res USING (maker_id);

/*query15*/
select 
    maker_id, avg(screen) as avg_size
from
    product
        inner join
    laptop USING (model)
group by maker_id
order by avg_size;



/*query16*/
select 
    maker_id, count(model) as mod_count
from
    makers
        inner join
    product USING (maker_id)
where
    product.type = 'PC'
group by maker_id
HAVING count(model) >= 3;


/*query17*/
drop  table if exists PrinterPc;
create table PrinterPC select distinct p2.maker_id, p2.model from
    product as p2
        inner join
    product as p1 ON (p1.type = 'Printer'
        and p1.maker_id = p2.maker_id)
where
    p2.type = 'PC';

select 
    @minRam:=min(ram)
from
    pc
        inner join
    PrinterPc USING (model);

select 
    @maxSpeed:=max(speed)
from
    pc
        inner join
    PrinterPc USING (model)
where
    ram = @minRam;

select 
    maker_id
from
    PrinterPc
        inner join
    (select 
        model
    from
        pc
    where
        speed = @maxSpeed and ram = @minRam) as res USING (model);
drop table PrinterPc;

/*query18*/

drop table if exists localeMaxPrices;
create table localeMaxPrices select model, price from
    pc
where
    price in (select 
            max(price) as maxPrice
        from
            pc) 
union select 
    model, price
from
    laptop
where
    price in (select 
            max(price) as maxPrice
        from
            laptop) 
union select 
    model, price
from
    pc
where
    price in (select 
            max(price) as maxPrice
        from
            printer);

select model from localeMaxPrices 
where price in (select max(price) from localeMaxPrices);


/*query19*/
drop  table if exists PcPrinter;
create table PcPrinter select distinct p1.maker_id, p1.model from
    product as p1
        inner join
    product as p2 ON (p1.type = 'PC'
        and p1.maker_id = p2.maker_id)
where
    p2.type = 'Printer';

select 
    maker_id, avg(hd)
from
    (select 
        maker_id, hd
    from
        pc
    inner join PcPrinter USING (model)) as res
group by maker_id;
 
drop  table if exists PcPrinter;

/*query20*/
drop  table if exists PcPrinter;
create table PcPrinter select distinct p1.maker_id, p1.model from
    product as p1
        inner join
    product as p2 ON (p1.type = 'PC'
        and p1.maker_id = p2.maker_id)
where
    p2.type = 'Printer';
select 
    maker_id, avg(hd)
from
    (select 
        maker_id, hd
    from
        pc
    inner join PcPrinter USING (model)) as res;
drop  table if exists PcPrinter; 


