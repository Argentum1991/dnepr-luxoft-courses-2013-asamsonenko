<%@ page import="java.util.concurrent.atomic.AtomicLong" %>
<%@ page import="com.luxoft.dnepr.courses.unit17.ConstParameters.SessionStats" %>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <style type="text/css">


        #logOut {
            top: 5px;
            margin-left: 90%
        }

        table {
            background: beige;
            font-size: 14pt;
            left: 35%;
            top: 25%;
            position: absolute;
        }

        tr.header {

            background: darkblue;
            color: orange;
            font-size: 16pt;
        }
    </style>
    <title>A Web Page</title>

</head>
<body>
<div id="logOut">
    <a href="logout">LogOut </a>
    <br/><br/>
    <a href="user">to User Page</a>
</div>


<table>
    <tr class="header">
        <td>Parameter</td>
        <td>Value</td>
    </tr>
    <c:forEach var="entry" items="${statLabel}">
        <tr>
            <td><c:out value="${statLabel[entry.key]}"/></td>
            <td><c:out value="${statValue[entry.key]}"/></td>
        </tr>
    </c:forEach>

</table>


</body>
</html>