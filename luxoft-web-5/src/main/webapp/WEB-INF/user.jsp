<%@ page import="com.luxoft.dnepr.courses.unit17.ConstParameters.ControlCustomers" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <style type="text/css">
        #logOut {
            top: 5px;
            margin-left: 90%
        }

        #welcomeUser {
            width: 50%;
            height: 150px;
            font-size: 28pt;
            position: absolute;
            left: 50%;
            top: 50%;
            margin-left: -25%;

            overflow: auto;
            background: beige;
            text-align: center;
        }

    </style>
    <title>A Web Page</title>

</head>
<body>
<div id="logOut">
    <a href="logout">LogOut </a>
    <br/><br/>
    <a href="admin">to Admin Page</a>
</div>

<div id="welcomeUser">
    <p>
        Hello, <c:out value='${sessionScope["login"] }'/>
    </p>
</div>
</body>
</html>