package com.luxoft.dnepr.courses.unit17.Listeners.Utils;


import com.luxoft.dnepr.courses.unit17.ConstParameters.Customers;
import com.luxoft.dnepr.courses.unit17.ConstParameters.SessionStats;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class StatMapHelper {
    Map<String, AtomicLong> statValue;

    public StatMapHelper(Map<String, AtomicLong> statValue) {
        this.statValue = statValue;
    }

    public void incrementSession(String role) {
        if (role.equals(Customers.ADMIN)) {
            role = SessionStats.ACTIVE_ADMIN_SESSION;
        } else {
            role = SessionStats.ACTIVE_USER_SESSION;
        }
        statValue.get(role).getAndIncrement();
        statValue.get(SessionStats.TOTAL_ACTIVE_SESSION).getAndIncrement();
    }

    public void decrementSession(String role) {
        if (role.equals(Customers.ADMIN)) {
            role = SessionStats.ACTIVE_ADMIN_SESSION;
        } else {
            role = SessionStats.ACTIVE_USER_SESSION;
        }
        statValue.get(role).getAndDecrement();
        statValue.get(SessionStats.TOTAL_ACTIVE_SESSION).getAndDecrement();
    }

    public void incrementRequest(String request) {
        statValue.get(request).getAndIncrement();
        statValue.get(SessionStats.TOTAL_REQUEST).getAndIncrement();
    }


}
