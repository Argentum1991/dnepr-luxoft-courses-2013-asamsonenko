package com.luxoft.dnepr.courses.unit17.ConstParameters;


public interface InitUtils {
    String STAT_MAP_HELPER = "statMapHelper";
    String FILTER_HELPER = "filterHelper";
}
