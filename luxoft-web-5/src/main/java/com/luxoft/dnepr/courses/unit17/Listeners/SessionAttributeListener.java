package com.luxoft.dnepr.courses.unit17.Listeners;


import com.luxoft.dnepr.courses.unit17.ConstParameters.ControlCustomers;
import com.luxoft.dnepr.courses.unit17.ConstParameters.InitUtils;
import com.luxoft.dnepr.courses.unit17.Listeners.Utils.StatMapHelper;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class SessionAttributeListener implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent hse) {
        String changedAttr = hse.getName();
        if (changedAttr.equals(ControlCustomers.queryROLE)) {
            StatMapHelper helper = (StatMapHelper) hse.getSession().getServletContext().getAttribute(InitUtils.STAT_MAP_HELPER);
            Object roleOb = hse.getSession().getAttribute(ControlCustomers.queryROLE);
            if (roleOb != null) {
                String role = (String) roleOb;
                helper.incrementSession(role);
            }
        }

    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent httpSessionBindingEvent) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent hse) {
        String changedAttr = hse.getName();
        if (changedAttr.equals(ControlCustomers.queryROLE)) {
            StatMapHelper helper = (StatMapHelper) hse.getSession().getServletContext().getAttribute(InitUtils.STAT_MAP_HELPER);
            Object roleOb = hse.getSession().getAttribute(ControlCustomers.queryROLE);
            if (roleOb != null) {
                String role = (String) roleOb;
                helper.incrementSession(role);
            }
        }

    }
}
