package com.luxoft.dnepr.courses.unit17.Listeners;

import com.luxoft.dnepr.courses.unit17.ConstParameters.ControlCustomers;
import com.luxoft.dnepr.courses.unit17.ConstParameters.InitUtils;
import com.luxoft.dnepr.courses.unit17.Listeners.Utils.StatMapHelper;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class ActiveSessionListener implements HttpSessionListener {

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        String role = (String) hse.getSession().getAttribute(ControlCustomers.queryROLE);
        StatMapHelper helper = (StatMapHelper) hse.getSession().getServletContext().getAttribute(InitUtils.STAT_MAP_HELPER);
        helper.decrementSession(role);
    }

    @Override
    public void sessionCreated(HttpSessionEvent hse) {
    }
}
