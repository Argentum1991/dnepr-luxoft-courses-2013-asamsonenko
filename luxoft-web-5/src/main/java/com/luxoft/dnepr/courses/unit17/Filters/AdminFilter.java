package com.luxoft.dnepr.courses.unit17.Filters;

import com.luxoft.dnepr.courses.unit17.ConstParameters.InitUtils;
import com.luxoft.dnepr.courses.unit17.ConstParameters.Paths;
import com.luxoft.dnepr.courses.unit17.Filters.Utils.FilterHelper;

import javax.servlet.*;
import java.io.IOException;

public class AdminFilter implements Filter {
    String contextPath;
    FilterHelper filterHelper;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        contextPath = filterConfig.getServletContext().getContextPath();
        filterHelper = (FilterHelper) filterConfig.getServletContext().getAttribute(InitUtils.FILTER_HELPER);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        filterHelper.estimateRanks(servletRequest, servletResponse,
                Paths.pathToAdmin, filterChain);
    }


    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
