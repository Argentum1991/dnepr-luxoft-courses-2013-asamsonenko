package com.luxoft.dnepr.courses.unit17.ConstParameters;


public interface Paths {
    String pathToAdmin = "/WEB-INF/admin/sessionData.jsp";
    String pathToUser = "/WEB-INF/user.jsp";
    String pathToIndex = "/index.jsp";

}
