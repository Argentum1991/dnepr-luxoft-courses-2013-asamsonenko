package com.luxoft.dnepr.courses.unit17.ConstParameters;


public interface InitMap {
    String STAT_VALUE = "statValue";
    String STAT_LABEL = "statLabel";
    String PATH_RANKS = "pathRanks";
    String CUSTOMER_RANKS = "customerRanks";

}
