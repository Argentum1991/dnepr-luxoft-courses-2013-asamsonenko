package com.luxoft.dnepr.courses.unit17.ConstParameters;

public interface SessionStats {
    String ACTIVE_USER_SESSION = "ACTIVE_USER_SESSION";
    String ACTIVE_ADMIN_SESSION = "ACTIVE_ADMIN_SESSION";
    String POST_REQUEST = "POST_REQUEST";
    String GET_REQUEST = "GET_REQUEST";
    String OTHER_REQUEST = "OTHER_REQUEST";
    String TOTAL_REQUEST = "TOTAL_REQUEST";
    String TOTAL_ACTIVE_SESSION = "ACTIVE_SESSION";
}