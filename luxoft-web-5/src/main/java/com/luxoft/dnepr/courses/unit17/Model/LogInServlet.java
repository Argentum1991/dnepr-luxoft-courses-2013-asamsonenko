package com.luxoft.dnepr.courses.unit17.Model;

import com.google.gson.Gson;
import com.luxoft.dnepr.courses.unit17.ConstParameters.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class LogInServlet extends HttpServlet {
    ConcurrentMap<String, Customer> passLogin;
    String contextPath;

    @Override
    public void init() throws ServletException {
        passLogin = new ConcurrentHashMap<>();
        String content = this.getServletContext().getInitParameter("users");
        contextPath = this.getServletContext().getContextPath();
        Gson gson = new Gson();
        Customer[] list = gson.fromJson(content, Customer[].class);
        for (int i = 0; i < list.length; i++) {
            passLogin.put(list[i].login, list[i]);
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doRequest(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doRequest(request, response);
    }

    public void doRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Customer customer = checkRequest(request);
        if (customer != null) {
            HttpSession session = request.getSession(true);
           // if (session.getAttribute(ControlCustomers.queryLOGIN) == null) {
                session.setAttribute(ControlCustomers.queryLOGIN, customer.login);
                session.setAttribute(ControlCustomers.queryROLE, customer.role);
            //}

            if (customer.role.equals(Customers.USER)) {
                forwardTo(Paths.pathToUser, request, response);
            } else
                forwardTo(Paths.pathToAdmin, request, response);
            return;
        }
       request.setAttribute(ControlCustomers.querySTATUS,"invalid");
        forwardTo(Paths.pathToIndex, request, response);
    }

    public Customer checkRequest(HttpServletRequest request) {
        String login = request.getParameter(ControlCustomers.queryLOGIN);
        String pass = request.getParameter(ControlCustomers.queryPASS);

        if (login != null && pass != null) {
            if (passLogin.containsKey(login)) {
                Customer man = passLogin.get(login);
                if (man.password.equals(pass)) {
                    return man;
                }
            }
        }
        return null;
    }

    public void forwardTo(String destination, HttpServletRequest request, HttpServletResponse response) throws IOException {

        //   destination = contextPath + destination;
        // destination="/admin/sessionData.jsp";
        //  destination="/sessionData.jsp";
        //response.sendRedirect(destination);
        try {
            RequestDispatcher toDestination = request.getRequestDispatcher(destination);
            toDestination.forward(request, response);
        } catch (ServletException ex) {
            ex.printStackTrace();
        }
    }

    class Customer {
        String login;
        String password;
        String role;
    }

}
