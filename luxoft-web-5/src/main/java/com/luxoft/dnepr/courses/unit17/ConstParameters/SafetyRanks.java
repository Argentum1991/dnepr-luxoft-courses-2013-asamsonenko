package com.luxoft.dnepr.courses.unit17.ConstParameters;

public interface SafetyRanks {
    short one = 1; //the lowest level of safety
    short two = 2;
    short three = 3; // the highest level of safety
}
