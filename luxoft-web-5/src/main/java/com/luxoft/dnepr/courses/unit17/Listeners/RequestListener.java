package com.luxoft.dnepr.courses.unit17.Listeners;

import com.luxoft.dnepr.courses.unit17.ConstParameters.InitUtils;
import com.luxoft.dnepr.courses.unit17.ConstParameters.SessionStats;
import com.luxoft.dnepr.courses.unit17.Listeners.Utils.StatMapHelper;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

public class RequestListener implements ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {

    }

    @Override
    public void requestInitialized(ServletRequestEvent requestEvent) {
        HttpServletRequest request = (HttpServletRequest) requestEvent.getServletRequest();
        StatMapHelper helper = (StatMapHelper) requestEvent.getServletContext().getAttribute(InitUtils.STAT_MAP_HELPER);
        String method = request.getMethod();
        if (method.equals("POST")) {
            helper.incrementRequest(SessionStats.POST_REQUEST);
        } else if (method.equals("GET")) {
            helper.incrementRequest(SessionStats.GET_REQUEST);
        } else {
            helper.incrementRequest(SessionStats.OTHER_REQUEST);
        }
    }


}
