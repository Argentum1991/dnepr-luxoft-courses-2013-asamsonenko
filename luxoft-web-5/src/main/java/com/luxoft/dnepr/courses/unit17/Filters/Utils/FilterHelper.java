package com.luxoft.dnepr.courses.unit17.Filters.Utils;


import com.luxoft.dnepr.courses.unit17.ConstParameters.ControlCustomers;
import com.luxoft.dnepr.courses.unit17.ConstParameters.Paths;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public class FilterHelper {
    Map<String, Short> customRanks;
    Map<String, Short> pathRanks;
    String contextPath;

    public FilterHelper(Map<String, Short> customRanks,
                        Map<String, Short> pathRanks, String contextPath) {
        this.customRanks = customRanks;
        this.pathRanks = pathRanks;
        this.contextPath = contextPath;
    }

    public synchronized void estimateRanks(ServletRequest servletRequest,
                                           ServletResponse servletResponse, String curPath, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(Paths.pathToIndex);
        //  String destination=contextPath+curPath;

        if (session != null) {
            String role = (String) session.getAttribute(ControlCustomers.queryROLE);
            if (customRanks.get(role) >= pathRanks.get(curPath)) {
                String destination = curPath;//+contextPath;
                requestDispatcher = request.getRequestDispatcher(destination);
            } else {
                session.invalidate();
                request.setAttribute(ControlCustomers.querySTATUS, "forbidden");
            }

        } else {
            request.setAttribute(ControlCustomers.querySTATUS, "invalid");
        }
        requestDispatcher.forward(request, response);
    }
}
