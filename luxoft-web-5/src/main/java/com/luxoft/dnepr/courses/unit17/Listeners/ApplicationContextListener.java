package com.luxoft.dnepr.courses.unit17.Listeners;

import com.luxoft.dnepr.courses.unit17.ConstParameters.*;
import com.luxoft.dnepr.courses.unit17.Filters.Utils.FilterHelper;
import com.luxoft.dnepr.courses.unit17.Listeners.Utils.StatMapHelper;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationContextListener implements ServletContextListener {
    Map<String, AtomicLong> statValue = new HashMap<>();
    Map<String, String> statLabel = new TreeMap<>();
    Map<String, Short> pathRanks = new HashMap<>();
    Map<String, Short> customerRanks = new HashMap<>();

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        statValue.put(SessionStats.ACTIVE_ADMIN_SESSION, new AtomicLong(0));
        statLabel.put(SessionStats.ACTIVE_ADMIN_SESSION, "Active Admin Sessions");

        statValue.put(SessionStats.ACTIVE_USER_SESSION, new AtomicLong(0));
        statLabel.put(SessionStats.ACTIVE_USER_SESSION, "Active User Sessions");

        statValue.put(SessionStats.TOTAL_ACTIVE_SESSION, new AtomicLong(0));
        statLabel.put(SessionStats.TOTAL_ACTIVE_SESSION, "Total Active Sessions");

        statValue.put(SessionStats.GET_REQUEST, new AtomicLong(0));
        statLabel.put(SessionStats.GET_REQUEST, "Total count of GET requests");

        statValue.put(SessionStats.POST_REQUEST, new AtomicLong(0));
        statLabel.put(SessionStats.POST_REQUEST, "Total count of POST requests ");

        statValue.put(SessionStats.OTHER_REQUEST, new AtomicLong(0));
        statLabel.put(SessionStats.OTHER_REQUEST, "Total count  Other requests ");

        statValue.put(SessionStats.TOTAL_REQUEST, new AtomicLong(0));
        statLabel.put(SessionStats.TOTAL_REQUEST, "Total count  of Requests");

        sce.getServletContext().setAttribute(InitMap.STAT_VALUE, statValue);
        sce.getServletContext().setAttribute(InitMap.STAT_LABEL, statLabel);

        pathRanks.put(Paths.pathToAdmin, SafetyRanks.three);
        pathRanks.put(Paths.pathToUser, SafetyRanks.two);
        pathRanks.put(Paths.pathToIndex, SafetyRanks.one);
        sce.getServletContext().setAttribute(InitMap.PATH_RANKS, pathRanks);
        customerRanks.put(Customers.ADMIN, SafetyRanks.three);
        customerRanks.put(Customers.USER, SafetyRanks.two);
        sce.getServletContext().setAttribute(InitMap.CUSTOMER_RANKS, customerRanks);

        FilterHelper filterHelper = new FilterHelper(customerRanks, pathRanks,
                sce.getServletContext().getContextPath());
        sce.getServletContext().setAttribute(InitUtils.FILTER_HELPER,
                filterHelper);

        StatMapHelper statMapHelper = new StatMapHelper(statValue);
        sce.getServletContext().setAttribute(InitUtils.STAT_MAP_HELPER,
                statMapHelper);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {


    }
}
