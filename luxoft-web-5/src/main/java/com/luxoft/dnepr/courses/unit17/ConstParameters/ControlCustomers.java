package com.luxoft.dnepr.courses.unit17.ConstParameters;


public interface ControlCustomers {

    String queryLOGIN = "login";
    String queryPASS = "password";
    String queryROLE = "role";
    String querySTATUS = "status";
}
