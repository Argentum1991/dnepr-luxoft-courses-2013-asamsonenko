package com.luxoft.dnepr.courses.unit22.dao;


import com.luxoft.dnepr.courses.unit22.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.unit22.exception.UserNotFound;

import com.luxoft.dnepr.courses.unit22.model.Redis;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

public class RedisDaoImplTest {
    Redis one;
    Redis two;
    RedisDaoImpl redisDao = new RedisDaoImpl();

    @Before
    public void prepare() {
        one = redisDao.create();
        two = redisDao.create();
        one.setWeight(225);
        two.setWeight(1000);
        one.setId(1L);
        two.setId(2L);
    }

    @Test
    public void isSave() throws SQLException, UserAlreadyExist {
        redisDao.save(one);
        redisDao.save(two);
        Assert.assertEquals(225, redisDao.get(1L).getWeight());
        Assert.assertEquals(1000, redisDao.get(2L).getWeight());
    }

    @Test
    public void isUpdate() throws SQLException, UserNotFound {
        one.setWeight(85);
        redisDao.update(one);
        Assert.assertEquals(85, redisDao.get(1L).getWeight());
    }
}
