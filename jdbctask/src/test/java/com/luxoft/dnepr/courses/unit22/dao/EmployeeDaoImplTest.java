package com.luxoft.dnepr.courses.unit22.dao;



import com.luxoft.dnepr.courses.unit22.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.unit22.exception.UserNotFound;
import com.luxoft.dnepr.courses.unit22.model.Employee;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.SQLException;

/*Написать тест который:
        - создает и сохраняет в БД 2 Employee
        - проверяет что в базе сохранилось 2 Employee
        - меняет salary одному из Employee
        - проверяет что в базе у этого Employee измененный salary
*/
public class EmployeeDaoImplTest {
    Employee one;
    Employee two;
    EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
    @Before
    public void prepare() {
        one = employeeDao.create();
        two = employeeDao.create();
        one.setSalary(7000);
        two.setSalary(3500);
        one.setId(1L);
        two.setId(2L);
    }

    @Test
    public void isSave() throws SQLException,UserAlreadyExist {
        employeeDao.save(one);
        employeeDao.save(two);
        Assert.assertEquals(7000, employeeDao.get(1L).getSalary());
        Assert.assertEquals(3500,employeeDao.get(2L).getSalary());
    }
    @Test
    public void isUpdate() throws SQLException,UserNotFound {
       one.setSalary(5000);
       employeeDao.update(one);
       Assert.assertEquals(5000,employeeDao.get(1L).getSalary());
    }
}
