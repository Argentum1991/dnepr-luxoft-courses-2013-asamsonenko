package com.luxoft.dnepr.courses.unit22.dao;


import com.luxoft.dnepr.courses.unit22.DataBaseUtil.ConnectionFactory;
import com.luxoft.dnepr.courses.unit22.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.unit22.exception.UserNotFound;
import com.luxoft.dnepr.courses.unit22.model.Entity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.*;
import java.util.Collections;
import java.util.Map;

public abstract class AbstractDao<E extends Entity> implements IDao<E> {
    protected static Long maxId;
    private Class<E> type;
    protected String entityTable;
    protected String entityName;
    //protected String queryMaxId = String.format("SELECT max(id) FROM %s", entityTable);
    //(artist_id, album_id, album_name)
    protected String queryInsertData;
    protected String queryUpdateData;
    protected String queryDeleteData;
    protected String queryEntity;

    public AbstractDao() {

    }

    protected void setType(Class<E> type) {
        this.type = type;
    }


    @Override
    public E create() {
        try {
            return type.getDeclaredConstructor().newInstance();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    @Override
    public E save(E e) throws UserAlreadyExist {

        int resSave = 0;

        try (
                Connection connection = ConnectionFactory.getInstance().getConnection();
                PreparedStatement saveEntity = connection.prepareStatement(queryInsertData);
        ) {


                /*
                java.util.Map map = connection.getTypeMap();
                map.put(String.format("%s\\.%s", entityTable, entityName), e.getClass());
                connection.setTypeMap(map);
                saveEntity.setObject(1, tmpMaxId+1);
                saveEntity.setObject(2, e);

                */
            fillPrepStatForSave(saveEntity, e);
            resSave = saveEntity.executeUpdate();

        } catch (SQLException sql) {
            sql.printStackTrace();
            throw new UserAlreadyExist();
        }
        return e;
    }

    protected abstract void fillPrepStatForSave(PreparedStatement ps, E e) throws SQLException;

    protected abstract void fillPrepStatForUpdate(PreparedStatement ps, E e) throws SQLException;

    protected abstract void setDataToFoundedEntity(ResultSet set, E e) throws SQLException;

    @Override
    public E update(E e) throws UserNotFound {

        int resUpd = 0;
        {
            try (
                    Connection connection = ConnectionFactory.getInstance().getConnection();
                    PreparedStatement updateEntity = connection.prepareStatement(queryUpdateData);
            ) {

                fillPrepStatForUpdate(updateEntity, e);

                resUpd = updateEntity.executeUpdate();

            } catch (SQLException sql) {
                sql.printStackTrace();
            }
        }
        if (resUpd == 0) {
            throw new UserNotFound();
        }
        return e;
    }

    @Override
    public E get(long id) {
        int resUpd = 0;
        E entity = null;
        try (
                Connection connection = ConnectionFactory.getInstance().getConnection();
                PreparedStatement psGet = connection.prepareStatement(queryEntity)
        ) {
            psGet.setLong(1, id);
            ResultSet resultSet = psGet.executeQuery();
            if (resultSet.first()) {
                entity = create();
                setDataToFoundedEntity(resultSet, entity);
            } else {
                return null;
            }

        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return entity;

    }

    @Override
    public boolean delete(long id) {
        int resDel = 0;

        try (
                Connection connection = ConnectionFactory.getInstance().getConnection();
                PreparedStatement psDel = connection.prepareStatement(queryDeleteData)
        ) {
            psDel.setLong(1, id);
            resDel = psDel.executeUpdate();
            if (resDel == 0) {
                return false;
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return true;
    }
}
