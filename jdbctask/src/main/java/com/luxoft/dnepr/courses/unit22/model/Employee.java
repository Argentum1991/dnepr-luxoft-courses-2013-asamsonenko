package com.luxoft.dnepr.courses.unit22.model;


import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class Employee extends Entity {
    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        super.readSQL(stream, typeName);
        salary = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        super.writeSQL(stream);
        stream.writeInt(salary);
    }
}
