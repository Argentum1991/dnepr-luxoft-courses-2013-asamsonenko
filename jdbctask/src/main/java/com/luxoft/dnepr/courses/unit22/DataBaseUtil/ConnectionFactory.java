package com.luxoft.dnepr.courses.unit22.DataBaseUtil;


import java.sql.*;

public class ConnectionFactory {
    private static ConnectionFactory factory = null;


    //Connection for h2base

    //  private String baseURL= "jdbc:h2:tcp//localhost/server~/test";

    private String baseURL = "jdbc:h2:~/test";
    private String user = "sa";
    private String password = "";


    //Connection for MySQL
     /*
    private String baseURL = "jdbc:mysql://localhost:3306/test";
    private String user = "root";
    private String password = "dostoevskiy9";
    */
    // DriverManager.getConnection("jdbc:h2:tcp://localhost/server~/dbname","username","password");
    public Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(baseURL, user, password);
        return connection;
    }

    private ConnectionFactory() {
    }

    public static ConnectionFactory getInstance() {
        if (factory == null) {
            factory = new ConnectionFactory();
        }
        return factory;
    }
}
