package com.luxoft.dnepr.courses.unit22.dao;


import com.luxoft.dnepr.courses.unit22.model.Employee;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeDaoImpl extends AbstractDao<Employee> {
    private Class<Employee> type;

    //protected String queryMaxId = String.format("SELECT max(id) FROM %s", entityTable);
    //(artist_id, album_id, album_name)

    public EmployeeDaoImpl() {
        super.setType(Employee.class);
        entityTable = "employee";
        entityName = "Employee";
        queryInsertData = String.format("INSERT INTO %s (salary) VALUES(?)", entityTable);
        queryUpdateData = String.format("UPDATE %s SET salary=? WHERE id=?", entityTable);
        queryDeleteData = String.format("DELETE FROM %s WHERE id=?", entityTable);
        queryEntity = String.format("Select * FROM %s WHERE id=?", entityTable);

    }

    @Override
    protected void fillPrepStatForSave(PreparedStatement ps, Employee employee) throws SQLException {
        ps.setInt(1, employee.getSalary());
    }

    @Override
    protected void fillPrepStatForUpdate(PreparedStatement ps, Employee employee) throws SQLException {
        ps.setInt(1, employee.getSalary());
        ps.setLong(2, employee.getId());

    }

    @Override
    protected void setDataToFoundedEntity(ResultSet set, Employee employee) throws SQLException {
        employee.setId(set.getLong("id"));
        employee.setSalary(set.getInt("salary"));
    }


}
