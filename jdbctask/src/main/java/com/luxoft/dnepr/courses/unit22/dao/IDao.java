package com.luxoft.dnepr.courses.unit22.dao;


import com.luxoft.dnepr.courses.unit22.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.unit22.exception.UserNotFound;
import com.luxoft.dnepr.courses.unit22.model.Entity;

public interface IDao<E extends Entity> {
    E create();

    E save(E e) throws UserAlreadyExist;

    E update(E e) throws UserNotFound;

    E get(long id);

    boolean delete(long id);
}