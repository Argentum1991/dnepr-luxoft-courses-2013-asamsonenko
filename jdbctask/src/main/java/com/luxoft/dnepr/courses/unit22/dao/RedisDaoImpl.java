package com.luxoft.dnepr.courses.unit22.dao;


import com.luxoft.dnepr.courses.unit22.model.Employee;
import com.luxoft.dnepr.courses.unit22.model.Entity;
import com.luxoft.dnepr.courses.unit22.model.Redis;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RedisDaoImpl extends AbstractDao<Redis> {
    private Class<Redis> type;

    public RedisDaoImpl() {
        super.setType(Redis.class);
        entityTable = "Redis";
        entityName = "Redis";
        //protected String queryMaxId = String.format("SELECT max(id) FROM %s", entityTable);
        //(artist_id, album_id, album_name)
        queryInsertData = String.format("INSERT INTO %s (weight) VALUES(?)", entityTable);
        queryUpdateData = String.format("UPDATE %s SET weight=? WHERE id=?", entityTable);
        queryDeleteData = String.format("DELETE FROM %s WHERE id=?", entityTable);
        queryEntity = String.format("Select * FROM %s WHERE id=?", entityTable);
    }

    @Override
    protected void fillPrepStatForSave(PreparedStatement ps, Redis redis) throws SQLException {
        ps.setInt(1, redis.getWeight());

    }

    @Override
    protected void fillPrepStatForUpdate(PreparedStatement ps, Redis redis) throws SQLException {
        ps.setInt(1, redis.getWeight());
        ps.setLong(2, redis.getId());
    }

    @Override
    protected void setDataToFoundedEntity(ResultSet set, Redis redis) throws SQLException {
        redis.setWeight(set.getInt("weight"));
    }


}
