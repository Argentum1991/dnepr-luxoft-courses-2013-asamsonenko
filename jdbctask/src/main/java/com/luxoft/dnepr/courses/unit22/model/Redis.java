package com.luxoft.dnepr.courses.unit22.model;


import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class Redis extends Entity {
    private int weight;

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        super.readSQL(stream, typeName);
        weight = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        super.writeSQL(stream);
        stream.writeInt(weight);
    }
}
