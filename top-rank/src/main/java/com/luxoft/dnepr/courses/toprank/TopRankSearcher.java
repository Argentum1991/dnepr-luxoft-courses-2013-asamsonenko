
package com.luxoft.dnepr.courses.toprank;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TopRankSearcher {
    static final String forUrl = "<.*?=.*?\\\"(\\w[^<]*\\w)\\\".*?>";//group1 is Url
    static final String gapBetweenTag = "(?s)(?<=>).*?(?=<)"; // group0 is gap)
    static final String wholeWord = "\\b([\\w\\-_'])+\\b";// group0 is word
    private static final double DEFAULT_DAMPING_FACTOR = 0.8;
    private static final int DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING = 10;
    ExecutorService service = Executors.newCachedThreadPool();
    Runnable dispatcherReverse;
    Runnable dispatcherGraph;
    Runnable dispatcherIndex;
    private HashMap<String, List<String>> tmpIndexMap = new HashMap<>();
    private HashMap<String, List<String>> tmpGraphMap = new HashMap<>();
    private HashMap<String, List<String>> tmpReverseMap = new HashMap<>();
    private HashMap<String, Double> tmpRankMap = new HashMap<>();
    private ArrayBlockingQueue<String> bufferGraph;
    private ArrayBlockingQueue<String> bufferReverse;
    private ArrayBlockingQueue<String> bufferIndex;
    private PatternAndGroupForSearch forGraph;
    private PatternAndGroupForSearch forIndex;
    private BufferTypeWriting buffTypeGraphRev = new BufferTypeWriting();
    private BufferTypeWriting buffTypeIndex = new BufferTypeWriting();

    public TopRankSearcher() {
        bufferGraph = new ArrayBlockingQueue<String>(30);
        bufferIndex = new ArrayBlockingQueue<String>(150);
        bufferReverse = new ArrayBlockingQueue<String>(30);
        forGraph = new PatternAndGroupForSearch(1);
        forGraph.add(Pattern.compile(forUrl), 1);
        forIndex = new PatternAndGroupForSearch(2);
        forIndex.add(Pattern.compile(gapBetweenTag), 0);
        forIndex.add(Pattern.compile(wholeWord), 0);
        buffTypeGraphRev.add(bufferGraph, true);
        buffTypeGraphRev.add(bufferReverse, false);
        buffTypeIndex.add(bufferIndex, true);

        dispatcherReverse =
                new DispatcherResultMap(tmpReverseMap, bufferReverse);
        dispatcherGraph =
                new DispatcherResultMap(tmpGraphMap, bufferGraph);
        dispatcherIndex =
                new DispatcherResultMap(tmpIndexMap, bufferIndex);
    }

    public TopRankResults execute(List<String> urls) {
        TopRankResults results = new TopRankResults();
        //<a href="http://udacity.com/cs101x/urank/nickel.html">
        Map<String, List<String>> tmpIndex = results.getIndex();
        Map<String, List<String>> tmpGraph = results.getGraph();
        Map<String, Double> tmpRank = results.getRanks();
        Map<String, List<String>> tmpReverse = results.getReverseGraph();
        execution(urls);
        countRank(DEFAULT_DAMPING_FACTOR, DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING);
        tmpIndex.putAll(tmpIndexMap);
        tmpGraph.putAll(tmpGraphMap);
        tmpReverse.putAll(tmpReverseMap);
        tmpRank.putAll(tmpRankMap);
        return results;
    }

    public void execution(List<String> urls) {
        service.execute(dispatcherGraph);
        service.execute(dispatcherIndex);
        service.execute(dispatcherReverse);
        for (int i = 0; i < urls.size(); i++) {
            String site = urls.get(i);
            service.execute(new DispatcherUrlContent(site));
            tmpRankMap.put(site.trim(), 0d);
        }

        boolean isDone = false;
        try {
            isDone = service.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
        }
        if (!isDone) {
            System.out.println("We must  close thread");
            service.shutdownNow();
        } else {
            System.out.println("Analysis of URL has been done");
        }
    }

    public TopRankResults execute(List<String> urls, double dampingFactor, int numberOfLoopsInRankComputing) {

        TopRankResults results = new TopRankResults();
        //<a href="http://udacity.com/cs101x/urank/nickel.html">
        Map<String, List<String>> tmpIndex = results.getIndex();
        Map<String, List<String>> tmpGraph = results.getGraph();
        Map<String, Double> tmpRank = results.getRanks();
        Map<String, List<String>> tmpReverse = results.getReverseGraph();
        execution(urls);
        countRank(dampingFactor, numberOfLoopsInRankComputing);
        tmpIndex.putAll(tmpIndexMap);
        tmpGraph.putAll(tmpGraphMap);
        tmpReverse.putAll(tmpReverseMap);
        tmpRank.putAll(tmpRankMap);
        return results;
    }

    public Map<String, Double> countRank(
            double dampingFactor, int numberOfLoopsInRankComputing) throws NullPointerException {
        double constFactor = 0;
        try {
            constFactor = (1 - dampingFactor) / tmpGraphMap.size();
        } catch (NullPointerException nux) {
            throw new NullPointerException("graphMap is empty");
        }
        Map<String, Double> oldRank = new HashMap<>(tmpRankMap);
        Map<String, Double> newRank = new HashMap<>();
        double countRank = 0d;
        String currentKey = null;

        for (int i = 0; i < numberOfLoopsInRankComputing; i++) {
            Iterator<String> itr = oldRank.keySet().iterator();

            while (itr.hasNext()) {
                countRank = 0d;
                currentKey = itr.next();
                // key of current Map is name of page where iterator is now
                // list of pages which have references to current page
                List<String> listRefPages = tmpGraphMap.get(currentKey);
                countRank = constFactor + dampingFactor * sumRank(listRefPages, oldRank);
                newRank.put(currentKey, countRank);
            }
            oldRank = new HashMap<>(newRank);
        }
        tmpRankMap.putAll(oldRank);

        return tmpRankMap;
    }

    public double sumRank(List<String> listRefPages, Map<String, Double> oldRank) {
        Double sumOfRanks = 0d; // sum of ranks those pages which has link to current page
        int howManytmpReverseGraph = 0;
        for (String item : listRefPages) {
            howManytmpReverseGraph = tmpReverseMap.get(item).size();
            sumOfRanks += oldRank.get(item) / howManytmpReverseGraph;
        }
        return sumOfRanks;
    }

    class PatternAndGroupForSearch {
        int[] numberOfGroup;
        Pattern[] patternSet;
        int index = -1;
        int size;

        PatternAndGroupForSearch(int size) {
            this.size = size;
            numberOfGroup = new int[size];
            patternSet = new Pattern[size];

        }

        public int getNumberOfGroup(int index) {
            return numberOfGroup[index];

        }

        public Pattern getPattern(int index) {
            return patternSet[index];
        }

        void add(Pattern newPattern, int groupForSearch) {
            if (index < size - 1) {
                index++;
                numberOfGroup[index] = groupForSearch;
                patternSet[index] = newPattern;
            }
        }

        public int getSize() {
            return size;
        }
    }

    class DispatcherUrlContent implements Runnable {
        URL myUrl = null;

        DispatcherUrlContent(String tmpURI) {
            try {
                myUrl = URI.create(tmpURI).toURL();
            } catch (MalformedURLException mux) {
            }
        }

        @Override
        public void run() {
            Scanner in = null;
            try {
                URLConnection connection = myUrl.openConnection();
                connection.connect();
                in = new Scanner(new BufferedInputStream(connection.getInputStream()));
            } catch (IOException ix) {
                return;
            }
            StringBuilder input = new StringBuilder("");
            while (in.hasNextLine()) {
                input.append(in.nextLine());
                input.append("\n");
            }

            try {
                /* ParseContent(String content, String currentPage,
                      PatternAndGroupForSearch myPattern,
                     BufferTypeWriting concreteBuffer) {*/
                ParseContent parseGraph = new ParseContent(input.toString(), myUrl.toString()
                        , true, forGraph, buffTypeGraphRev);
                ParseContent parseIndex = new ParseContent(input.toString(), myUrl.toString(),
                        false, forIndex, buffTypeIndex);
                service.execute(parseGraph);
                service.execute(parseIndex);
            } catch (Exception ex) {
                return;
            }
        }
    }

    class BufferTypeWriting {
        List<Boolean> invList = new ArrayList<>();
        //inversion- dataOnPage,CurrentPage:GraphMap,Index
        List<ArrayBlockingQueue<String>> queueList = new ArrayList<>();

        void add(ArrayBlockingQueue<String> item, boolean typeWriting) {
            invList.add(typeWriting);
            queueList.add(item);
        }
    }

    class ParseContent implements Runnable {
        String myContent;
        String currentPage;
        PatternAndGroupForSearch myPattern;
        boolean printCurPage;
        BufferTypeWriting concreteBuffer;

        ParseContent(String content, String currentPage,
                     boolean printCurPage, PatternAndGroupForSearch myPattern,
                     BufferTypeWriting concreteBuffer) {
            myContent = content;
            this.currentPage = currentPage;
            this.myPattern = myPattern;
            this.concreteBuffer = concreteBuffer;
            this.printCurPage = printCurPage;
            //  System.out.println(content);
        }

        public void parsing(int numberOfPattern, String tmpContent) {
            int index = numberOfPattern;
            Pattern concretePattern = myPattern.getPattern(index);
            Matcher concreteMatcher = concretePattern.matcher(tmpContent);
            String tmpData = null;

            while (concreteMatcher.find()) {
                tmpData = concreteMatcher.group(myPattern.getNumberOfGroup(index));
                if (numberOfPattern < myPattern.size - 1 & tmpData.length() > 0) {
                    parsing(index + 1, tmpData);
                } else {
                    putInBuffer(tmpData);

                }
            }
            if (index == myPattern.size - 1) {
                if (printCurPage) {
                    putInBuffer("null");
                    // this is for safety, that every currentPage will be situated in
                    // GraphMap or ReverseGraphMap
                }
            }
        }

        public void run() {
            parsing(0, myContent);
        }

        public void putInBuffer(String tmpData) {
            //System.out.println(tmpData);
            //System.out.println(tmpData);

            for (int i = 0; i < concreteBuffer.invList.size(); i++) {
                try {   //for reverse
                    if (tmpData.equals("null")) {
                        concreteBuffer.queueList.get(i).put(currentPage + ";" + tmpData);
                    } else {
                        //for reverseGraph
                        if (!concreteBuffer.invList.get(i)) {
                            concreteBuffer.queueList.get(i).put(currentPage + ";" + tmpData);
                        } else {
                            //for graph, index
                            concreteBuffer.queueList.get(i).put(tmpData + ";" + currentPage);
                        }
                    }
                } catch (InterruptedException iex) {
                }
            }

        }
    }

    class DispatcherResultMap implements Runnable {
        HashMap<String, List<String>> concreteMap;
        ArrayBlockingQueue<String> concreteBuffer;

        DispatcherResultMap(HashMap<String, List<String>> concreteMap, ArrayBlockingQueue<String> concreteBuffer) {
            this.concreteMap = concreteMap;
            this.concreteBuffer = concreteBuffer;
        }

        @Override
        public void run() {
            while (true) {
                //  keyData - is currentPage in ReverseMap
                // is tmpWord or Link founded on the currentPage in GraphMap and ReverseMap
                String keyData = null;
                //valueData is link which is placed on currentPage - in reverseMap
                //valueData   is currentPage -in graphMap and IndexMap
                String valueData = null;
                String[] cortege;
                try {
                    cortege = concreteBuffer.take().split(";");
                    keyData = cortege[0];
                    valueData = cortege[1];
                    //     System.out.println(keyData + ":" + valueData);
                    addDataToConcreteMap(keyData, valueData);

                } catch (InterruptedException ix) {
                }
            }
        }

        void addDataToConcreteMap(String keyData, String valueData) {
            List<String> listData;
            if (concreteMap.containsKey(keyData)) {
                listData = new ArrayList<>(concreteMap.get(keyData));
            } else {
                listData = new ArrayList<>();
            }
            if (!valueData.equals("null")) {
                if (!listData.contains(valueData)) {
                    listData.add(valueData);
                }
            }
            concreteMap.put(keyData, listData);

        }

    }


}

