package com.luxoft.dnepr.courses.toprank;


import java.lang.Double;
import java.lang.Integer;
import java.lang.String;
import java.util.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TopRankExecutor {
    private double dampingFactor;
    private int numberOfLoopsInRankComputing;
    private Map<String, Integer> linkFromPage = new HashMap<>();



    public TopRankExecutor(double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
    }

    public TopRankResults execute(Map<String, String> urlContent) {
        TopRankResults results = new TopRankResults();
        //<a href="http://udacity.com/cs101x/urank/nickel.html">


        Map<String, List<String>> tmpIndex = results.getIndex();
        Map<String, List<String>> tmpGraph = results.getGraph();
        Map<String, Double> tmpRank = results.getRanks();

        Iterator<Map.Entry<String, String>> urlIterator = urlContent.entrySet().iterator();
        Map.Entry<String, String> entryPairs = null;
        String input = null;

        String originUrl = null;

        while (urlIterator.hasNext()) {
            entryPairs = urlIterator.next();
            input = entryPairs.getValue();
            originUrl = entryPairs.getKey();
            tmpRank.put(originUrl.trim(), 0d);
            if (!tmpGraph.containsKey(originUrl)){
            tmpGraph.put(originUrl.trim(),new ArrayList<String>());
            }
            addWordToIndexMap(tmpIndex, input, originUrl);
            addLinkToGraphMap(tmpGraph, tmpRank, input, originUrl);
        }

       tmpRank= countRank(tmpGraph, tmpRank);
        System.out.println(tmpRank);
        System.out.println(linkFromPage);
        return results;
    }

    public void addWordToIndexMap(Map<String, List<String>> indexMap, String input, String curPage) {
        String gapBetweenTag = "(?s)(?<=>).+?(?=<)";// group0 is gap)
        String wholeWord = "\\b([\\w\\-_'])+\\b";// group0 is word
        Pattern forGap = Pattern.compile(gapBetweenTag);
        Pattern forWholeWord = Pattern.compile(wholeWord);
        Matcher matForGap = forGap.matcher(input);

        String foundGap = null;
        String tmpWord = null;
        List<String> listUrl = null;
        while (matForGap.find()) {
            foundGap = matForGap.group();
            Matcher matForWord = forWholeWord.matcher(foundGap);

            while (matForWord.find()) {
                tmpWord = matForWord.group();

                if (indexMap.containsKey(tmpWord)) {
                    listUrl = new ArrayList<>(indexMap.get(tmpWord));
                } else {
                    listUrl = new ArrayList<>();
                }

                if (curPage != null) {
                    try {
                        if (!listUrl.contains(curPage.trim())) {
                            listUrl.add(curPage.trim());
                        }
                    } catch (NullPointerException nux) {
                    }
                }
                indexMap.put(tmpWord, listUrl);
            }
        }

    }

    public void addLinkToGraphMap(Map<String, List<String>> graphMap,
                                  Map<String, Double> rankMap,
                                  String input, String curPage) {
        String patternForUrl = "<.*?=.*?\\\"(\\w[^<]*\\w)\\\".*?>";//group1 is Url
        Pattern patForUrl = Pattern.compile(patternForUrl);
        Matcher matForUrl = patForUrl.matcher(input);
        String tmpUrl = null;
        List<String> listUrl;

        int amountLinkFromPage = 0;
        while (matForUrl.find()) {
            tmpUrl = matForUrl.group(1);
            amountLinkFromPage++;
            if (graphMap.containsKey(tmpUrl)) {
                listUrl = new ArrayList<>(graphMap.get(tmpUrl));
            } else {
                listUrl = new ArrayList<>();
            }
            if (curPage != null) {
                try {
                    if (!listUrl.contains(curPage)) {
                        listUrl.add(curPage);
                    }
                } catch (NullPointerException nux) {
                }
            }

            graphMap.put(tmpUrl, listUrl);


        }
        linkFromPage.put(curPage, amountLinkFromPage);


    }

    public Map<String,Double> countRank(Map<String, List<String>> graphMap, Map<String, Double> rankMap) throws NullPointerException {
        double constFactor = 0;
        try {
            constFactor = (1 - dampingFactor) / graphMap.size();
        } catch (NullPointerException nux) {
            throw new NullPointerException("graphMap is empty");
        }
        Map<String, Double> oldRank = new HashMap<>(rankMap);
        Map<String, Double> newRank = new HashMap<>();
        double countRank = 0d;
        String  currentKey = null;

        for (int i = 0; i < numberOfLoopsInRankComputing; i++) {
            Iterator<String> itr = oldRank.keySet().iterator();

            while (itr.hasNext()) {
                countRank = 0d;
                currentKey = itr.next();
                // key of current Map is name of page where iterator is now
                // list of pages which have references to current page
                List<String> listRefPages = graphMap.get( currentKey);
                countRank = constFactor + dampingFactor*sumRank(listRefPages, oldRank);
                newRank.put(currentKey, countRank);
            }

            oldRank = new HashMap<>(newRank);

        }
        rankMap.putAll(oldRank);

        return rankMap;
    }

    public double sumRank(List<String> listRefPages, Map<String, Double> oldRank) {
        Double sumOfRanks = 0d; // sum of ranks those pages which has link to current page
        int howManyLinkFromPage=0;
        for (String item : listRefPages) {
            howManyLinkFromPage=linkFromPage.get(item);
            sumOfRanks+= oldRank.get(item)/howManyLinkFromPage;
        }
        return sumOfRanks;
    }
}


