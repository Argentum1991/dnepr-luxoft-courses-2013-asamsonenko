CREATE TABLE IF NOT EXISTS makers (
maker_id int NOT NULL AUTO_INCREMENT,
maker_name varchar(50) NOT NULL,
maker_adress varchar(200),
CONSTRAINT pk_makers PRIMARY KEY(maker_id)
);


INSERT INTO makers(maker_name, maker_adress) VALUES ('A', 'AdressA');
INSERT INTO makers(maker_name, maker_adress) VALUES ('B', 'AdressB');
INSERT INTO makers(maker_name, maker_adress) VALUES ('C', 'AdressC');
INSERT INTO makers(maker_name, maker_adress) VALUES ('D', 'AdressD');
INSERT INTO makers(maker_name, maker_adress) VALUES ('E', 'AdressE');



ALTER TABLE product
ADD COLUMN maker_id INT NOT NULL;
update product set maker_id=maker;

ALTER TABLE product
DROP maker,
ADD CONSTRAINT fk_product_makers 
FOREIGN KEY (maker_id)
REFERENCES makers(maker_id);
  
  
CREATE TABLE IF NOT EXISTS printer_type(
type_id INT NOT NULL AUTO_INCREMENT,
type_name VARCHAR(50) NOT NULL,
CONSTRAINT pk_printer_type PRIMARY KEY (type_id)
);


INSERT INTO printer_type(type_name ) VALUES ('Matrix');
INSERT INTO printer_type(type_name ) VALUES ('Laser');
INSERT INTO printer_type(type_name ) VALUES ('Jet');


ALTER TABLE printer
ADD COLUMN type_id INT NOT NULL;
UPDATE printer SET type_id=`type`;

ALTER TABLE printer
DROP `type`,
ADD CONSTRAINT fk_printer_type
FOREIGN KEY (type_id)
REFERENCES printer_type(type_id );

ALTER TABLE printer
CHANGE color color CHAR(1) DEFAULT'y' NOT NULL;

ALTER TABLE pc 
ADD INDEX ind_pc_price (price);
ALTER TABLE laptop
ADD INDEX ind_laptop_price (price);
ALTER TABLE printer
ADD INDEX ind_printer_price (price);
