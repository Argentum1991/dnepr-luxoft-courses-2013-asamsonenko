

INSERT INTO product(maker, model, `type`) VALUES (1, '1232', 'PC');
INSERT INTO product(maker, model, `type`) VALUES (1, '1233', 'PC');
INSERT INTO product(maker, model, `type`) VALUES (1, '1276', 'Printer');
INSERT INTO product(maker, model, `type`) VALUES (1, '1298', 'Laptop');
INSERT INTO product(maker, model, `type`) VALUES (1, '1401', 'Printer');
INSERT INTO product(maker, model, `type`) VALUES (1, '1408', 'Printer');
INSERT INTO product(maker, model, `type`) VALUES (1, '1752', 'Laptop');
INSERT INTO product(maker, model, `type`) VALUES (2, '1121', 'PC');
INSERT INTO product(maker, model, `type`) VALUES (2, '1750', 'Laptop');
INSERT INTO product(maker, model, `type`) VALUES (3, '1321', 'Laptop');
INSERT INTO product(maker, model, `type`) VALUES (4, '1288', 'Printer');
INSERT INTO product(maker, model, `type`) VALUES (4, '1433', 'Printer');
INSERT INTO product(maker, model, `type`) VALUES (5, '1260', 'PC');
INSERT INTO product(maker, model, `type`) VALUES (5, '1434', 'Printer');
INSERT INTO product(maker, model, `type`) VALUES (5, '2112', 'PC');
INSERT INTO product(maker, model, `type`) VALUES (5, '2113', 'PC');

INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (1,  '1232', 500, 64,  5.00, '12x', 600.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (2,  '1121', 750, 128, 14.0, '40x', 850.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (3,  '1233', 500, 64,  5.00, '12x', 600.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (4,  '1121', 600, 128, 14.0, '40x', 850.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (5,  '1121', 600, 128, 8.00, '40x', 850.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (6,  '1233', 750, 128, 20.0, '50x', 950.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (7,  '1232', 500, 32,  10.0, '12x', 400.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (8,  '1232', 450, 64,  8.00, '24x', 350.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (9,  '1232', 450, 32,  10.0, '24x', 350.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (10, '1260', 500, 32,  10.0, '12x', 350.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (11, '1233', 900, 128, 40.0, '40x', 980.0);
INSERT INTO pc(id, model, speed, ram, hd, cd, price) VALUES (12, '1233', 800, 128, 20.0, '50x', 970.0);

INSERT INTO laptop(id, model, speed, ram, hd, screen, price) VALUES (1, '1298', 350, 32 , 4.00, 11, 700.00);
INSERT INTO laptop(id, model, speed, ram, hd, screen, price) VALUES (2, '1321', 500, 64 , 8.00, 12, 970.00);
INSERT INTO laptop(id, model, speed, ram, hd, screen, price) VALUES (3, '1750', 750, 128, 12.0, 14, 1200.0);
INSERT INTO laptop(id, model, speed, ram, hd, screen, price) VALUES (4, '1298', 600, 64 , 10.0, 15, 1050.0);
INSERT INTO laptop(id, model, speed, ram, hd, screen, price) VALUES (5, '1752', 750, 128, 10.0, 14, 1150.0);
INSERT INTO laptop(id, model, speed, ram, hd, screen, price) VALUES (6, '1298', 450, 64 , 10.0, 12, 950.00);

INSERT INTO printer(id, model, color, `type`, price) VALUES (1, '1276', 'n', 1, 400.0);
INSERT INTO printer(id, model, color, `type`, price) VALUES (2, '1433', 'y', 2, 270.0);
INSERT INTO printer(id, model, color, `type`, price) VALUES (3, '1434', 'y', 2, 290.0);
INSERT INTO printer(id, model, color, `type`, price) VALUES (4, '1401', 'n', 3, 150.0);
INSERT INTO printer(id, model, color, `type`, price) VALUES (5, '1408', 'n', 3, 270.0);
INSERT INTO printer(id, model, color, `type`, price) VALUES (6, '1288', 'n', 1, 400.0);


