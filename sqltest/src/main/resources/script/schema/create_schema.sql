
CREATE TABLE IF NOT EXISTS product (
	maker INT,
	model VARCHAR(50) NOT NULL,
	`type` VARCHAR(50) NOT NULL

);

CREATE TABLE IF NOT EXISTS pc (
	id INT,
	model VARCHAR(50) NOT NULL,
	speed SMALLINT,
	ram SMALLINT,
	hd FLOAT,
	cd VARCHAR(10),
	price DOUBLE

);

CREATE TABLE IF NOT EXISTS laptop (
	id INT,
	model VARCHAR(50),
	speed SMALLINT,
	ram SMALLINT,
	hd FLOAT,
	screen SMALLINT,
	price DOUBLE	
);

CREATE TABLE IF NOT EXISTS printer (
	id INT,
	model VARCHAR(50) NOT NULL,
	color CHAR(1) NOT NULL DEFAULT 'y', 
	`type` INT NOT NULL,
	price DOUBLE
);