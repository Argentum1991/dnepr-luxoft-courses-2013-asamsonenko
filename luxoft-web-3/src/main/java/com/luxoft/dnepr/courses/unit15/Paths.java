package com.luxoft.dnepr.courses.unit15;


public interface Paths {
    String pathToAdmin="/admin/sessionData.jsp";
    String pathToUser="/user.jsp";
    String pathToIndex="/index.jsp";
}
