package com.luxoft.dnepr.courses.unit15.Listeners;


import com.luxoft.dnepr.courses.unit15.Constants;

import javax.servlet.http.HttpSessionEvent;
import java.util.concurrent.atomic.AtomicLong;

public final class GetConcreteSessions {
    public static AtomicLong getAdminSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_ADMIN_SESSION);
    }
    public static AtomicLong getUserSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_USER_SESSION);
    }
}
