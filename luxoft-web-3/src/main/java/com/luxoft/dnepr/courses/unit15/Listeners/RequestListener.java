package com.luxoft.dnepr.courses.unit15.Listeners;

import com.luxoft.dnepr.courses.unit15.Constants;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;

public class RequestListener implements ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {

    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        HttpServletRequest request = (HttpServletRequest) servletRequestEvent.getServletRequest();
        String method = request.getMethod();
        if (method.equals("POST")) {
            returnPOST(servletRequestEvent).getAndIncrement();
        } else if (method.equals("GET")) {
            returnGET(servletRequestEvent).getAndIncrement();
        } else {
            returnOTHER(servletRequestEvent).getAndIncrement();
        }
    }

    public AtomicLong returnPOST(ServletRequestEvent servletRequestEvent) {
        return (AtomicLong) servletRequestEvent.getServletContext().getAttribute(Constants.POST_REQUEST);
    }

    public AtomicLong returnGET(ServletRequestEvent servletRequestEvent) {
        return (AtomicLong) servletRequestEvent.getServletContext().getAttribute(Constants.GET_REQUEST);
    }

    public AtomicLong returnOTHER(ServletRequestEvent servletRequestEvent) {
        return (AtomicLong) servletRequestEvent.getServletContext().getAttribute(Constants.OTHER_REQUEST);
    }
}
