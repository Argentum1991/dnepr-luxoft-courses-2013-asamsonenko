package com.luxoft.dnepr.courses.unit15.Listeners;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class ActiveSessionListener implements HttpSessionListener {

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
      String role=(String)  hse.getSession().getAttribute("role");
        if (role.equals("admin")){
           GetConcreteSessions.getAdminSessions(hse).getAndDecrement();
        } else {
           GetConcreteSessions. getUserSessions(hse).getAndDecrement();
        }

    }


    @Override
    public void sessionCreated(HttpSessionEvent hse) {
}
}
