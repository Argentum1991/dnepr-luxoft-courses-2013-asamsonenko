package com.luxoft.dnepr.courses.unit15;

import com.google.gson.Gson;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Checker implements Filter {
    ConcurrentMap<String, Customer> passLogin;
    FilterConfig fc;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.fc = filterConfig;
        passLogin = new ConcurrentHashMap<>();
        String content = fc.getServletContext().getInitParameter("users");
        Gson gson = new Gson();
        Customer[] list = gson.fromJson(content, Customer[].class);
        for (int i = 0; i < list.length; i++) {
            passLogin.put(list[i].login, list[i]);
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String role = checkRequest(request);
        if (role != null) {
            HttpSession session = request.getSession(true);
            synchronized (session) {
                if (session.getAttribute("login") == null) {
                    session.setAttribute("login", request.getParameter("login"));
                    session.setAttribute("role", role);
                }
            }

            if (role.equals("user")) {
                forwardTo(Paths.pathToUser, request, response);
            } else
                forwardTo(Paths.pathToAdmin, request, response);
            return;
        }
        request.setAttribute("status", "invalid");
        forwardTo(Paths.pathToIndex, request, response);
    }

    @Override
    public void destroy() {

    }

    public String checkRequest(HttpServletRequest request) {
        String login = request.getParameter("login");
        String pass = request.getParameter("password");

        if (login != null && pass != null) {
            if (passLogin.containsKey(login)) {
                Customer man = passLogin.get(login);
                if (man.password.equals(pass)) {
                    return man.role;
                }
            }
        }
        return null;
    }

    public void forwardTo(String destination, HttpServletRequest request, HttpServletResponse response) throws IOException {
        // String contextPath= request.getServletContext().getContextPath();
        //destination = contextPath + destination;
        // destination="/admin/sessionData.jsp";
        //  destination="/sessionData.jsp";
        //response.sendRedirect(destination);
        try {
            RequestDispatcher toDestination = request.getRequestDispatcher(destination);
            toDestination.forward(request, response);
        } catch (ServletException ex) {
            ex.printStackTrace();
        }
    }

    class Customer {
        String login;
        String password;
        String role;
    }
}




