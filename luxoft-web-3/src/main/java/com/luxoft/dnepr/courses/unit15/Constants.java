package com.luxoft.dnepr.courses.unit15;

public interface Constants {
    String ACTIVE_USER_SESSION = "ACTIVE_USER_SESSION";
    String ACTIVE_ADMIN_SESSION = "ACTIVE_ADMIN_SESSION";
    String POST_REQUEST="POST_REQUEST";
    String GET_REQUEST="GET_REQUEST";
    String OTHER_REQUEST="OTHER_REQUEST";

}