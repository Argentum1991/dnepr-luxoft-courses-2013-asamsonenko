package com.luxoft.dnepr.courses.unit15.Listeners;


import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class SessionAttributeListener implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent hse) {
        String changedAttr = hse.getName();
        if (changedAttr.equals("role")) {
            Object roleOb = hse.getSession().getAttribute("role");
            if (roleOb != null) {
                String role = (String) roleOb;
                if (role.equals("admin")) {
                    GetConcreteSessions.getAdminSessions(hse).getAndIncrement();
                } else {
                    GetConcreteSessions.getUserSessions(hse).getAndIncrement();
                }
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent httpSessionBindingEvent) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
