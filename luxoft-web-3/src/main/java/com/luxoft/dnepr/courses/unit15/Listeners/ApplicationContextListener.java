package com.luxoft.dnepr.courses.unit15.Listeners;

import com.luxoft.dnepr.courses.unit15.Constants;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(Constants.ACTIVE_ADMIN_SESSION, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_USER_SESSION, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.POST_REQUEST, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.GET_REQUEST, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.OTHER_REQUEST,new AtomicLong(0));

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(Constants.ACTIVE_ADMIN_SESSION);
        sce.getServletContext().removeAttribute(Constants.ACTIVE_USER_SESSION);
        sce.getServletContext().removeAttribute(Constants.POST_REQUEST);
        sce.getServletContext().removeAttribute(Constants.OTHER_REQUEST);
        sce.getServletContext().removeAttribute(Constants.GET_REQUEST);

    }
}
