<html>
<head>
    <style type="text/css">
        body {
            background-color: rgb(245, 245, 245);
        }

        #container {
            position: absolute;
            width: 200px;
            height: 150px;
            left: 50%;
            top: 50%;
            margin-left: -125px;
            margin-top: -60px;
            background: rgb(142, 167, 219);
            padding: 10px;
            overflow: auto;
        }

        #right {
            float: right;
        }

        #right, #right input {
            width: 110px;
        }

        #left {
            margin-right: 110px;
            margin-left: 0;
        }

        #left p, #right input {
            border: 0px;
            font-family: 'baskerville old face', calligrapher;
            font-size: 12pt;
            padding: 0px 0px;
            margin: 5px;
            height: 16pt;
        }
        [type="password"]{
         font-size: 10pt;
        }

        #result {
            color: red;
            margin-top: 5px;
            clear: right;
            font-size: 10pt;
        }
        #result span.start{
             color:white;
         }
        #result span.wrong{
            color:red;
        }

    </style>
    <title>A Web Page</title>

</head>
<body>
<div id="container">

    <div id="right">
        <form method="POST" action="user">
            <input type="text" name="login">
            <input type="password" name="password" >
            <input type="submit" value="Sign In" style="width: 65px;height:16pt">
        </form>
    </div>

    <div id="left">
        <p>Login:</p>

        <p>Password:</p>
    </div>
    <div id="result">
        <%
            Object status= request.getAttribute("status");

            if (status==null){
                out.print("<span class=\"start\">Please enter your login and pass</span>");
            } else {
                out.print("<span class=\"wrong\">Wrong login or password</span>");
            }

        %>
    </div>
</div>

</body>
</html>