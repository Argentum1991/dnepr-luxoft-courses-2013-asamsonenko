<%@ page import="java.util.concurrent.atomic.AtomicLong" %>
<%@ page import="com.luxoft.dnepr.courses.unit15.Constants" %>
<html>
<head>
    <style type="text/css">


        #logOut {
            top: 5px;
            margin-left: 90%
        }

        table {
            background: beige;
            font-size: 14pt;
            left: 35%;
            top: 25%;
            position: absolute;
        }
        tr.header {

            background: darkblue;
            color: orange;
            font-size: 16pt;
        }
    </style>
    <title>A Web Page</title>

</head>
<body>
<div id="logOut">
    <a href="logout">LogOut </a>
</div>

    <%
        ServletContext context = request.getSession().getServletContext();
        AtomicLong activeUserSession = (AtomicLong) context.getAttribute(Constants.ACTIVE_USER_SESSION);
        AtomicLong activeAdminSession = (AtomicLong) context.getAttribute(Constants.ACTIVE_ADMIN_SESSION);
        Long totalSession = activeAdminSession.addAndGet(activeUserSession.longValue());
        AtomicLong postReq = (AtomicLong) context.getAttribute(Constants.POST_REQUEST);
        AtomicLong getReq = (AtomicLong) context.getAttribute(Constants.GET_REQUEST);
        AtomicLong otherReq = (AtomicLong) context.getAttribute(Constants.OTHER_REQUEST);
        Long totalReq = postReq.longValue() + getReq.longValue() + otherReq.longValue();
        out.print("<table>");
        out.print("<tr class=\"header\"><td>Parameter </td><td>Value</td></tr>");
        out.print("<tr><td>Active Sessions </td> <td>" + totalSession + "</td></tr>");
        out.print("<tr><td>Active User Sessions </td> <td>" + activeUserSession + "</td></tr>");
        out.print("<tr><td>Active Admin Sessions </td> <td>" + activeAdminSession + "</td></tr>");
        out.print("<tr><td>Total count  of Requests </td> <td>" + totalReq + "</td></tr>");
        out.print("<tr><td>Total count of POST requests </td> <td>" + postReq + "</td></tr>");
        out.print("<tr><td>Total count of GET requests </td> <td>" + getReq + "</td></tr>");
        out.print("<tr><td>Total count  Other requests </td> <td>" + otherReq + "</td></tr>");
        out.print("</table>");
    %>

</body>
</html>