package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;

public class RedisDaoImpl<E extends Entity> extends AbstractDao {
    public RedisDaoImpl() {
        super(Redis.class);
    }
}
