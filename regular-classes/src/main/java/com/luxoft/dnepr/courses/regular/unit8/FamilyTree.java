package com.luxoft.dnepr.courses.regular.unit8;

import java.io.*;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FamilyTree implements Externalizable {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FamilyTree that = (FamilyTree) o;


        if (root != null ? !root.equals(that.root) : that.root != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = root != null ? root.hashCode() : 0;

        return result;
    }

    private Person root;
    private transient Date creationTime;

    public FamilyTree() {
        setCreationTime(new Date());
    }

    public FamilyTree(Person root) {
        this();
        setRoot(root);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        String input = in.readUTF();

   //     System.out.println("Family Tree:" + input);

        String gapBetweenTag = "(?s)(?<=\\{)root:\\{(.+)(?=\\})";// group0 is gap)
        Pattern patForRoot = Pattern.compile(gapBetweenTag);
        Matcher matcher = patForRoot.matcher(input);


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        String entry = null;
        if (matcher.find()) {
            entry = matcher.group(1);

        }
        oos.writeUTF(entry);
        oos.flush();
        baos.flush();
        oos.close();

        byte[] byteToArray = baos.toByteArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(byteToArray);
        ObjectInputStream ois = new ObjectInputStream(bais);
        Person root1=new Person();
        root1.readExternal(ois);
        setRoot(root1);

    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(this.toString());

    }

    public Person getRoot() {
        return root;
    }

    private void setRoot(Person root) {
        this.root = root;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    private void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public String toString() {
        return String.format("{root:%s}", root.toString());
    }
}
