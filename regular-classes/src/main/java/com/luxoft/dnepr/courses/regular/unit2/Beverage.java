package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct implements Product {
    boolean nonAlcoholic;

    public  Beverage(String code, String name, double price, boolean nonAlcoholic){
    this.code=code;
     this.name=name;
     this.price=price;
     this.nonAlcoholic=nonAlcoholic;
    }
    @Override
    public boolean equals(Object o) {

        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        if (nonAlcoholic == beverage.nonAlcoholic) return true;

        return false;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nonAlcoholic ? 1 : 0);
        return result;
    }
    public  boolean isNonAlcoholic(){
        return nonAlcoholic;
    }


    public void setNonAlcoholic(boolean nonAlcoholic){
    this.nonAlcoholic=nonAlcoholic;
    }
}
