package com.luxoft.dnepr.courses.regular.unit18.Model.DataBase;

public interface DataBaseControllerMethods {
    public void addNewData(String[] sources);
     public String isPrepareForGame(int elementIndex);
}
