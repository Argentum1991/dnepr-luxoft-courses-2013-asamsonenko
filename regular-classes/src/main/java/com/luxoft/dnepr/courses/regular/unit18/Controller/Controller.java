package com.luxoft.dnepr.courses.regular.unit18.Controller;


import com.luxoft.dnepr.courses.regular.unit18.Model.Constants.Commands;
import com.luxoft.dnepr.courses.regular.unit18.Model.DataBase.ControlDataBase;
import com.luxoft.dnepr.courses.regular.unit18.Model.GameMaker;
import com.luxoft.dnepr.courses.regular.unit18.Model.Utils.FromJSON;
import com.luxoft.dnepr.courses.regular.unit18.Model.Utils.ReaderFromExternalSource;
import com.luxoft.dnepr.courses.regular.unit18.View.GUIMaker;
import com.luxoft.dnepr.courses.regular.unit18.View.ViewCommands;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Controller implements Commands {

    ExecutorService threadPoolExecutor;
    List<Future<Boolean>> tasks;
    List<String> controlConfig;
    ViewCommands guiMaker;
    GameMaker gameMaker;
    ControlDataBase controlDB;

    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.init();
        controller.loopGame();
    }

    public static void startFromTest(ViewCommands guiMaker) {
        Controller controller = new Controller();
        controller.init(guiMaker);
        controller.loopGame();
    }

    public void init() {
        if (guiMaker == null) {
            guiMaker = new GUIMaker();
        }
        threadPoolExecutor = Executors.newCachedThreadPool();
        tasks = new ArrayList<>();
        controlConfig = new ArrayList<>();
        tasks.add(threadPoolExecutor.submit(
                new ReaderFromExternalSource("config_src.txt", controlConfig)));
        waitForTermination(tasks);
        FromJSON fromJSON = new FromJSON();
        DataBaseSources dataBaseSources =
                fromJSON.fromListJSONConfig(controlConfig, DataBaseSources.class);
        controlDB = new ControlDataBase(threadPoolExecutor, tasks);
        controlDB.addNewData(dataBaseSources.sources);
        gameMaker = new GameMaker(threadPoolExecutor, tasks);
        gameMaker.loadConfig("config_msg.txt");
        waitForTermination(tasks);

        String res = controlDB.isPrepareForGame(0);
        if (res == null) {
            System.exit(0);
        }
    }

    public void init(ViewCommands guiMaker) {
        this.guiMaker = guiMaker;
        init();
    }

    public void waitForTermination(List<Future<Boolean>> tasks) {
        try {
            for (Future<Boolean> future : tasks) {
                future.get();
            }
        } catch (ExecutionException | InterruptedException iex) {
            iex.printStackTrace();
        }
    }

    public void loopGame() {
        boolean isContinue = true;
        gameMaker.initGame(controlDB);
        placeTextInGUI(gameMaker.getExplanation());
        placeTextInGUI(gameMaker.showHelp());

        while (isContinue) {
            placeTextInGUI(gameMaker.showTalkToUser());
            placeNextQuestion();
            isContinue = getUserAnswer();
        }
    }

    void placeTextInGUI(String[] array) {
        for (String entry : array) {
            guiMaker.placeData(entry);
        }
    }

    void placeTextInGUI(String string) {
        guiMaker.placeData(string);
    }

    public boolean getUserAnswer() {
        //return true, if continue game respectively user's answer
        String answer = guiMaker.getUserAnswers().toUpperCase();
        return answerController(answer);
    }

    public void placeNextQuestion() {
        placeTextInGUI(gameMaker.getNextWord());
    }

    public boolean answerController(String answer) {
        //return true, if continue game respectively user's answer
        if (answer.equals(HELP)) {
            placeTextInGUI(gameMaker.showHelp());
            return true;
        } else if (answer.equals(ABOUT)) {
            placeTextInGUI(gameMaker.showAbout());
        } else if (answer.equals(EXIT)) {
            System.exit(0);
        } else if (answer.equals(RESULT)) {
            placeTextInGUI(gameMaker.getResult());
            return false;
        } else if (answer.equals(YES) || answer.equals(NO)) {
            placeTextInGUI(gameMaker.addAnswer(answer));
        } else {
            String dataForPrint = ("You've typed: " + answer + ", but such command is absent!");
            placeTextInGUI(dataForPrint);
        }
        return true;
    }

    class DataBaseSources {
        String[] sources;
    }


}
