package com.luxoft.dnepr.courses.regular.unit8;


import java.io.*;


public class Serializer {
    public static void serialize(File file, FamilyTree entity) {
        try {
            FileOutputStream toFile = new FileOutputStream(file);
            ObjectOutputStream obj = new ObjectOutputStream(toFile);
            obj.writeObject(entity);
        } catch (IOException ix) {
            ix.printStackTrace();
        }

    }

    public static FamilyTree deserialize(File file) {

        FamilyTree restoreFamily=null;
        try{
        FileInputStream fis=new FileInputStream(file);
        ObjectInputStream ois=new ObjectInputStream(fis);

         restoreFamily=(FamilyTree) ois.readObject();
        }
        catch (IOException |ClassNotFoundException ix){ix.printStackTrace();
        }
        return restoreFamily;

    }
}

