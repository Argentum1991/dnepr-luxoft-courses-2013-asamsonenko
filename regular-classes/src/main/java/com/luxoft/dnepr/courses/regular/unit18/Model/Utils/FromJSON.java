package com.luxoft.dnepr.courses.regular.unit18.Model.Utils;


import com.google.gson.Gson;

import java.util.List;

public class FromJSON {
    private Gson gson = new Gson();


    public <E extends Object> E fromListJSONConfig(List<String> listConfig, Class<E> object) {
        StringBuilder stringBuilder = new StringBuilder("");
        for (String entry : listConfig) {
            stringBuilder.append(entry);
        }
        @SuppressWarnings("unchecked") E result = (E) gson.fromJson(stringBuilder.toString(), object);
        return result;
    }

}
