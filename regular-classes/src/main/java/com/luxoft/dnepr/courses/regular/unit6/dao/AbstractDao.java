package com.luxoft.dnepr.courses.regular.unit6.dao;


import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;


import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class AbstractDao<E extends Entity> implements IDao<E> {
    protected static Long maxId;
    private Class<E> type;

    public AbstractDao(Class<E> type) {
        this.type = type;
        if (maxId == null) {
            maxId = new Long(0);
        }
    }

    public E create() {
        try {
            return type.getDeclaredConstructor().newInstance();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override

    public  E save(E entity) throws EntityAlreadyExistException {
    @SuppressWarnings("checked")E savedEntity=(E)EntityStorage.getInstance().putInStorage(entity);
    return savedEntity;
    }

    @Override
    public E update(E entity) throws EntityNotFoundException {
    @SuppressWarnings("checked")E updatedEntity= (E)EntityStorage.getInstance().updateInStorage(entity);
    return updatedEntity;
    }

    @Override
    public E get(long id) {
        @SuppressWarnings("checked")E returnedEntity= (E)EntityStorage.getInstance().getElement(id);
     return  returnedEntity;
    }

    @Override
    public boolean delete(long id) {
   return  EntityStorage.getInstance().remove(id);
    }
}
