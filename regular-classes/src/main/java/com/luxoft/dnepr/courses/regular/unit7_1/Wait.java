package com.luxoft.dnepr.courses.regular.unit7_1;

class Wait  {
  static   Wait one;
    public synchronized void waitForSeaWeather() throws InterruptedException {
        while (3 == 3) {
            wait();
        }
    }
    public synchronized void waitForTheDawn() throws InterruptedException {
        while (3 == 3) {
            wait(15000);
        }
    }
    public static Wait getInstance(){
    if (one==null){
        one =new Wait();
        return one;
    }else
    return one;
    }
}