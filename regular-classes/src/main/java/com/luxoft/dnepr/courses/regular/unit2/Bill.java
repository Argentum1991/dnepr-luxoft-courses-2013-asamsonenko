package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    CompositeProduct mainComposite = new CompositeProduct();


    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */

    public void append(Product product) {
        mainComposite.add(product);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        return mainComposite.getPrice();
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
      List<Product> list= mainComposite.getChildProducts();
      Collections.sort(list,new descSort());
      System.out.println(list);
      return list;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

    class descSort implements Comparator<Product> {
        public int compare(Product p1, Product p2) {
            Double res = (p2.getPrice() - p1.getPrice());
            int resInt = res.intValue();
            return resInt;
        }
    }

}
