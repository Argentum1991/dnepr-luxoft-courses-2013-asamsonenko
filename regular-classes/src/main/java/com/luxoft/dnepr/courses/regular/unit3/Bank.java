package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;

public class Bank implements BankInterface {
    private Map<Long, UserInterface> users;

    public Bank(String expectedJavaVersion) throws IllegalJavaVersionError{
      String actualJavaVersion=System.getProperty("java.version");
        if (!(expectedJavaVersion.equals(actualJavaVersion))) {
             String toMsg=String.format("Sorry, but actual version JRE %s" +
                   "does not match to the expected %s",
                   actualJavaVersion,expectedJavaVersion);
            throw new IllegalJavaVersionError(actualJavaVersion,
                    expectedJavaVersion, toMsg );
            //String actualJavaVersion, String expectedJavaVersion,String message)
        }
    }
    public Bank(){}




    @Override
    public Map<Long, UserInterface> getUsers() {
        return this.users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        Locale.setDefault(Locale.CANADA);

        if (!users.containsKey(fromUserId)) {
            //format(“I have %,.2f bugs to fix.”, 476578.09876);
            //%[argument number][flags][width][.precision]type
            String message = String.format("User with id %d is absent in database", fromUserId);
            throw new NoUserFoundException(fromUserId, message);
        }
        if (!users.containsKey(toUserId)) {
            String message = String.format("User with id %d is absent in database", toUserId);
            throw new NoUserFoundException(toUserId, message);
        }

        UserInterface tmpFromUser = users.get(fromUserId);
        UserInterface tmpToUser = users.get(toUserId);

        //TransactionException(String message)
        try {
            tmpFromUser.getWallet().checkWithdrawal(amount);
        } catch (WalletIsBlockedException wallBlock) {
            String message = String.format("User '%s' wallet is blocked", tmpFromUser.getName());
            throw new TransactionException(message);
        } catch (InsufficientWalletAmountException ex) {
            String userName = tmpFromUser.getName();
            BigDecimal userWalletAmount = tmpFromUser.getWallet().getAmount();
            String message = String.format("User '%s' has insufficient funds (%.2f < %.2f)",
                    userName, userWalletAmount, amount);
            throw new TransactionException(message);
        }

        try {
            tmpToUser.getWallet().checkTransfer(amount);
        } catch (WalletIsBlockedException wallBlock) {
            String message = String.format("User '%s' wallet is blocked", tmpToUser.getName());
            throw new TransactionException(message);
        } catch (LimitExceededException limEx) {
            String userName = tmpToUser.getName();
            BigDecimal maxAmount = tmpToUser.getWallet().getMaxAmount();
            BigDecimal userWalletAmount = tmpToUser.getWallet().getAmount();
            String message = String.format("User '%s' wallet limit exceeded(%.2f + %.2f > %.2f)",
            userName, userWalletAmount, amount, maxAmount);
            throw new TransactionException(message);
        }
        tmpFromUser.getWallet().withdraw(amount);
        tmpToUser.getWallet().transfer(amount);
        Locale.setDefault(Locale.GERMAN);
        /* Locale locale  = new Locale("en", "UK");
           String pattern = "###.##";
           DecimalFormat decimalFormat = (DecimalFormat)
           NumberFormat.getNumberInstance(locale);
           decimalFormat.applyPattern(pattern);
        String $amount=decimalFormat.format(amount);
          Locale.setDefault(Locale.CANADA);*/

    }

}
