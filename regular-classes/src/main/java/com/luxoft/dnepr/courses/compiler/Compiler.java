package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Compiler {

    public static final boolean DEBUG = true;

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        String postFix=toPostFix(input);
        Pattern patForNumber=Pattern.compile("\\d{1,}\\.{0,1}\\d*");
        Matcher matForNumber=null;

        Stack<Double> number=new Stack<Double>();

        Pattern pat=Pattern.compile("[\\+\\/\\*-]");
        Matcher mat=pat.matcher(postFix);
        int start=0;
        int end=0;
        while (mat.find()){
            String tocken=mat.group();
            end=mat.start();
            matForNumber=patForNumber.matcher(postFix).region(start, end);

            while (matForNumber.find()){
                number.push(Double.valueOf(matForNumber.group()));
                addCommand(result,VirtualMachine.PUSH,Double.valueOf(matForNumber.group()));
            }
            start=end;
            if (tocken.equals("+")){
                addCommand(result,VirtualMachine.ADD);}
            else if  (tocken.equals("-")){
                addCommand(result,VirtualMachine.SWAP);
                addCommand(result,VirtualMachine.SUB);}
            else if  (tocken.equals("*")){
                addCommand(result,VirtualMachine.MUL);}
            else if  (tocken.equals("/")){
                addCommand(result,VirtualMachine.SWAP);
                addCommand(result,VirtualMachine.DIV);}
        }

    addCommand(result,VirtualMachine.PRINT);

        return result.toByteArray();
    }

    static String toPostFix(String inFix) {
        String RegEx = "(\\()|(\\))|(\\d{1,}\\.{0,1}\\d*)|([\\+-])|([\\/\\*])";

        StringBuilder postFix = new StringBuilder("");
        inFix="("+inFix+")";
        // group1=(, group2=), group3=double number (46.63 for example), group4=+-,group=*/
        Stack<String> tmpStack = new Stack<String>();
        Pattern pattern = Pattern.compile(RegEx);
        Matcher matcher = pattern.matcher(inFix);
        while (matcher.find()) {
            int i = 0;
            do {
                i++;
            } while ((matcher.group(i) == null));
            if (i == 1) {
                tmpStack.push(matcher.group(i));
            } else if (i == 2) {
                do {
                    postFix.append(tmpStack.pop());
                } while (!tmpStack.peek().equals("(") && !tmpStack.empty());
                tmpStack.pop();
            } else if (i == 3) {
                postFix.append(matcher.group(i));
                postFix.append(" ");
            } else if (i == 4) {
                while (!tmpStack.peek().equals("(") && !(tmpStack.empty())) {
                    postFix.append(tmpStack.pop());
                    postFix.append(" ");
                }
                tmpStack.push(matcher.group(i));
            } else {
                tmpStack.push(matcher.group(5));
            }

        }

    return postFix.toString();
}

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
