package com.luxoft.dnepr.courses.regular.unit18.Model;


import com.luxoft.dnepr.courses.regular.unit18.Model.Constants.Commands;
import com.luxoft.dnepr.courses.regular.unit18.Model.DataBase.DataBaseGameMethods;
import com.luxoft.dnepr.courses.regular.unit18.Model.Utils.FromJSON;
import com.luxoft.dnepr.courses.regular.unit18.Model.Utils.ReaderFromExternalSource;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class GameMaker {
    int baseSize;
    DataBaseGameMethods dataBase;
    ExecutorService threadPoolExecutor;
    List<Future<Boolean>> tasks;
    List<String> listConfig;
    Msg msgInfo;
    Stat statGame;
    String pathToConfig;

    public GameMaker(ExecutorService threadPoolExecutor, List<Future<Boolean>> tasks) {
        this.tasks = tasks;
        this.threadPoolExecutor = threadPoolExecutor;
    }

    public void loadConfig(String pathToConfig) {
        this.pathToConfig = pathToConfig;
        listConfig = new ArrayList<>();
        tasks.add(threadPoolExecutor.submit(new ReaderFromExternalSource(pathToConfig, listConfig)));
    }

    public void initGame(DataBaseGameMethods dataBase) {
        this.dataBase = dataBase;
        this.baseSize = dataBase.getBaseSize();

        statGame = new Stat();
        FromJSON fromJSON = new FromJSON();
        msgInfo = fromJSON.fromListJSONConfig(listConfig, Msg.class);
    }


    public String showAbout() {
        return msgInfo.about;
    }

    public String[] showHelp() {
        return msgInfo.help;
    }

    public String showTalkToUser() {
        int index = (int) (Math.random() * (msgInfo.talkToUser.length - 1));
        return msgInfo.talkToUser[index];
    }

    public String getNextWord() {
        int index = (int) (Math.random() * (baseSize - 1));
        String word = dataBase.getWord(index);
        return word;
    }

    public String getExplanation() {
        return msgInfo.explanation;
    }

    public String getResult() {
        String result = msgInfo.result;
        return result + statGame.getResult() + " words";
    }

    public String addAnswer(String answer) {
        if (answer.equals(Commands.YES)) {
            statGame.incrementKnownWords();
        } else {
            statGame.incrementUnknownWords();
        }
        return "Answer is added";
    }

    class Msg {
        String about;
        String[] help;
        String explanation;
        String[] talkToUser;
        String result;

    }

    class Stat {
        float knownWords;
        float unknownWords;

        void incrementKnownWords() {
            knownWords++;
        }

        void incrementUnknownWords() {
            unknownWords++;
        }

        int getResult() {
            int res = 0;
            if (knownWords != 0) {
                res = (int) ((knownWords / (unknownWords + knownWords)) * baseSize);
            }
            return res;
        }
    }
}

