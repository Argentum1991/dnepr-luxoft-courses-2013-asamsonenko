package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct implements Product {
    double weight;

    public Bread(String code, String name, double price, double weight) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Bread bread = (Bread) o;
        if (Double.compare(bread.weight, weight) == 0) return true;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = weight != +0.0d ? Double.doubleToLongBits(weight) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
