package com.luxoft.dnepr.courses.regular.unit8;

public enum Gender {
    
    MALE, FEMALE;
}
