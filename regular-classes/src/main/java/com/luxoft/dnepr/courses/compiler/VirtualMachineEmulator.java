package com.luxoft.dnepr.courses.compiler;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Stack;

/**
 * Emulator for physical VM. Can be used for testing purposes.
 * Use {@link #create(byte[], OutputStream)} method to obtain instance of this class.
 */
public class VirtualMachineEmulator implements VirtualMachine {
	
	private static final int MAX_STACK_SIZE = 20;

	public static VirtualMachine create(byte[] byteCode, OutputStream out) {
		return new VirtualMachineEmulator(byteCode, out);
	}
	
	private byte[] byteCode;
	private PrintWriter out;
	private Stack<Double> stack;

	private VirtualMachineEmulator(byte[] byteCode, OutputStream out) {
		if (byteCode == null) {
			throw new NullPointerException("Bytecode is NULL");
		}
		this.byteCode = byteCode;
		this.stack = new Stack<Double>();
		this.out = new PrintWriter(out);
	}

	public void run() {
		for (int i = 0; i < byteCode.length; i++) {
			byte instruction = byteCode[i];
			int offset = i;
			
			double arg = 0;
			if (instruction == PUSH) {
				arg = readDoubleAt(i+1);
				i += 8;
			}
			executeInstruction(instruction, arg, offset);
		}
	}

	private double readDoubleAt(int i) {
		if ((i + 8) >= byteCode.length) {
			throw new IllegalArgumentException("Operation PUSH expects single argument of type DOUBLE");
		}
		long bits = (((long)byteCode[i] << 56) +
                ((long)(byteCode[i + 1] & 255) << 48) +
		        ((long)(byteCode[i + 2] & 255) << 40) +
                ((long)(byteCode[i + 3] & 255) << 32) +
                ((long)(byteCode[i + 4] & 255) << 24) +
                ((byteCode[i + 5] & 255) << 16) +
                ((byteCode[i + 6] & 255) <<  8) +
                ((byteCode[i + 7] & 255) <<  0));
		return Double.longBitsToDouble(bits);

	}

	private void executeInstruction(byte instruction, double arg, int offset) {
		switch (instruction) {
		case PUSH:
			checkStack(1);
			stack.push(arg);
			return;
		case POP:
			checkStack(-1);
			stack.pop();
			return;
		case SWAP:
			Double arg1 = stack.pop();
			Double arg2 = stack.pop();
			stack.push(arg1);
			stack.push(arg2);
			return;
		case DUP:
			checkStack(1);
			stack.push(stack.lastElement());
			return;
		case ADD:
			checkStack(-2);
			stack.push(stack.pop() + stack.pop());
			return;
		case MUL:
			checkStack(-2);
			stack.push(stack.pop() * stack.pop());
			return;
		case SUB:
			checkStack(-2);
			double sub = stack.pop();
			stack.push(sub - stack.pop());
			return;
		case DIV:
			checkStack(-2);
			double div = stack.pop();
			stack.push(div / stack.pop());
			return;
		case PRINT:
			checkStack(-1);
			out.println(stack.pop());
			out.flush();
			return;
		default:
			throw new IllegalArgumentException("Bad bytecode point: " + instruction + " at offset " + offset);	
		}
	}

	private void checkStack(int sizeDelta) {
		int newLen = stack.size() + sizeDelta;
		if (newLen > MAX_STACK_SIZE) {
			throw new StackOverflowError("Size of the stack is limited by " + MAX_STACK_SIZE + " items");
		}
		if (newLen < 0) {
			throw new StackOverflowError("Trying to remove elements from the empty stack");
		}
	}
}
