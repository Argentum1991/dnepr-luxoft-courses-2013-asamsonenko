package com.luxoft.dnepr.courses.regular.unit2;

import java.util.GregorianCalendar;

/**
 * Main entry point to the program 'Shop'.
 */
public class Shop {
    public static void main(String[] args) throws CloneNotSupportedException {
        ProductFactory productFactory = new ProductFactory();

        Bill bill = new Bill();

        //buy 2 pieces of bread
        Bread bread = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);
        //TODO uncomment lines below when implementation is ready.
//        bill.append(bread);
//        Bread anotherBread = (Bread)bread.clone();
//        anotherBread.setPrice(15);
//        bill.append(anotherBread);

        //3 bottles of Cola
        Beverage cola = productFactory.createBeverage("beverage1", "Coca-cola", 5, true);
//        bill.append(cola);
//        bill.append((Product)cola.clone());
//        bill.append((Product)cola.clone());

        //and 1 book about Java
        Book javaBook = productFactory.createBook("book1", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
//        bill.append(javaBook);

        System.out.println(bill);
    }
}
