package com.luxoft.dnepr.courses.regular.unit18.View;


import java.io.Console;
import java.io.PrintWriter;
import java.util.Scanner;

public class GUIMaker implements ViewCommands {
    PrintWriter printWriter;
    Scanner scanner;

    public GUIMaker() {
        Console console = System.console();
        if (console != null) {
            PrintWriter printWriter = console.writer();
            scanner = new Scanner(console.reader());
        }
    }

    @Override
    public void placeData(String data) {
        printWriter.println(data);
    }

    @Override
    public String getUserAnswers() {
        return scanner.nextLine();
    }
}
