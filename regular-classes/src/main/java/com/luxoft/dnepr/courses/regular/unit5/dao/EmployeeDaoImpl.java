package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

public class EmployeeDaoImpl<E extends Entity> extends AbstractDao {
    public EmployeeDaoImpl(){
        super(Employee.class);

        }
    }
