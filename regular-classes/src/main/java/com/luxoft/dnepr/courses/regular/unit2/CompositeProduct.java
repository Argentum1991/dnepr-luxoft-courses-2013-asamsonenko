package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;


/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private int mainAmount;// for count each item
    private List<Product> childProducts = new ArrayList<Product>();
    private Map<Product, Map<Double, Integer>> originalContent = new HashMap<Product, Map<Double, Integer>>();

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        if (!childProducts.isEmpty()) {
            return childProducts.get(0).getCode();
        }
        return null;
    }

    public Map<Product, Map<Double, Integer>> getOriginalContent() {
        return originalContent;
    }
    public List<Product> getChildProducts(){
        getPrice();
        return childProducts;
    }
    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        if (!childProducts.isEmpty()) {
            return childProducts.get(0).getClass().toString();
        }
        return null;
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */

    public double getDiscount(int curAmount) {
        double discount = 0;
        if (curAmount == 2) {
            discount = 0.05;
        } else if (curAmount == 3) {
            discount = 0.1;
        }
        return discount;
    }


    public void add(Product product) {
        if (product instanceof CompositeProduct) {
            CompositeProduct tmpProduct = (CompositeProduct) product;
            Map<Product, Map<Double, Integer>> tmpMap = tmpProduct.getOriginalContent();
            Iterator iter = tmpMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<Product, Map<Double, Integer>> tmpEntry = (Map.Entry<Product, Map<Double, Integer>>) iter.next();
                Product leaf = tmpEntry.getKey();
                analyzeContent(leaf);
                getPrice();
            }
        } else {
            analyzeContent(product);
        }

    }

    public void analyzeContent(Product product) {
        if (originalContent.containsKey(product)) {
            Map<Double, Integer> priceMap = originalContent.get(product);
            if (priceMap.containsKey(product.getPrice())) {
                int amount = priceMap.get(product.getPrice());
                priceMap.put(product.getPrice(), amount + 1);
                return;
            } else {
                priceMap.put(product.getPrice(), 1);
                return;
            }
        } else {
            originalContent.put(product, new HashMap<Double, Integer>());
            Map<Double, Integer> newPriceMap = originalContent.get(product);
            newPriceMap.put(product.getPrice(), 1);

        }
    }

    public void remove(Product product) {
        // originalContent consists of pairs key=Product,value=Map<Price,Amount>
        //so each item of it can comply couple values inside map<price,amount>
        //when we are going to remove element from originalContent
        // we try to find, if product in originalContent has such price as requested for deleting parameter
        // if it exists then
        //if it's amount is more than one, than we remove the last item of price
        //otherwise we remove product from originalContent
        // else, we  remove product from originalContent too

        if (originalContent.containsKey(product)) {
            Map<Double, Integer> priceMap = originalContent.get(product);
            if (priceMap.containsKey(product.getPrice())) {
                int amount = priceMap.get(product.getPrice());
                if (amount > 1) {
                    priceMap.put(product.getPrice(), amount - 1);
                    return;
                } else {
                    priceMap.remove(product.getPrice());
                    if (!priceMap.isEmpty()) {
                        return;
                    }
                }
            }
            originalContent.remove(product);
            getPrice();
           // childProducts.remove(product);
        }
    }

    @Override
    public double getPrice() {
        childProducts.clear();
        double sumPrice = 0;
        mainAmount = 0;
        Iterator outerItr = originalContent.entrySet().iterator();
        while (outerItr.hasNext()) {
            Map.Entry<Product, Map<Double, Integer>> pairsOut = (Map.Entry<Product, Map<Double, Integer>>) outerItr.next();
            Iterator innerItr = pairsOut.getValue().entrySet().iterator();
            int tmpAmount = 0;
            double tmpPrice = 0;
            while (innerItr.hasNext()) {
                Map.Entry<Double, Integer> pairsIn = (Map.Entry<Double, Integer>) innerItr.next();
                tmpAmount += pairsIn.getValue();
                tmpPrice += pairsIn.getKey() * pairsIn.getValue();
            }
            childProducts.add(new forBill(tmpAmount,pairsOut.getKey().getName(),tmpPrice));
            double discount = getDiscount(tmpAmount);
            mainAmount += tmpAmount;

            sumPrice = sumPrice + tmpPrice * (1 - discount);

        }
        return sumPrice;
    }


    public int getAmount() {
        getPrice();
        return mainAmount;
    }

    class forBill extends AbstractProduct {
        Integer amount;

        forBill(int amount, String name, Double price) {
            this.amount = amount;
            this.name = name;
            this.price = price;
        }

        public int getAmount() {
            return amount;
        }

        @Override
        public String toString() {
            return getName() + " * " + getAmount() + " = " + getPrice();
        }
    }

}
