package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;
    private BigDecimal amountToWithdraw;
    private BigDecimal amountToTransfer;

    public Wallet(WalletStatus status, BigDecimal maxAmount, Long id, BigDecimal amount) {
        this.status = status;
        this.maxAmount = maxAmount;
        this.id = id;
        this.amount = amount;
    }
   public Wallet(){}
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public WalletStatus getStatus() {
        return status;
    }

    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw)
            throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(id,"Wallet is blocked");
        }
        if (amountToWithdraw.compareTo(amount) > 0) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount,
                    "Amount to Withdraw is greater than balance ");
        }


    }


    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        this.amount = this.amount.subtract(amountToWithdraw);
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer)
            throws WalletIsBlockedException, LimitExceededException {
        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(this.id,"Wallet is blocked");
        }
       if ((amountToTransfer.add(this.amount).compareTo(this.maxAmount)) > 0) {
            throw new LimitExceededException(this.id, amountToTransfer, this.amount, "Amount " +
                    "to transfer  amount in wallet are greater than settled limit");
         }

    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
    this.amount=this.amount.add(amountToTransfer);
    }

    public BigDecimal getAmountToWithdraw() {
        return amountToWithdraw;
    }

    public void setAmountToWithdraw(BigDecimal amountToWithdraw) {
        this.amountToWithdraw = amountToWithdraw;
    }

    public BigDecimal getAmountToTransfer() {
        return amountToTransfer;
    }

    public void setAmountToTransfer(BigDecimal amountToTransfer) {
        this.amountToTransfer = amountToTransfer;
    }


}
