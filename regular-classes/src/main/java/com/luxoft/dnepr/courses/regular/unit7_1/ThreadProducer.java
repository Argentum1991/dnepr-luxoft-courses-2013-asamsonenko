package com.luxoft.dnepr.courses.regular.unit7_1;

public final class ThreadProducer {

    private ThreadProducer() {
    }

    public static Thread getNewThread() {
        return new Thread();
    }

    public static Thread getRunnableThread() {
        Thread thread = new Thread() {
            public void run() {
                while (3 == 3) {
                }
            }
        };
        thread.start();
        return thread;
    }

    public static Thread getBlockedThread() {

        Thread active = new Thread() {
            public void run() {
                block();
            }
        };

        Thread blocked = new Thread() {
            public void run() {
                block();
            }
        };
        active.start();
        try {
            Thread.currentThread().sleep(100);
        } catch (InterruptedException ix) {
        }
        blocked.start();
        try {
            Thread.currentThread().sleep(100);
        } catch (InterruptedException ix) {
        }
        Thread.currentThread().yield();
        return blocked;
    }

    public static Thread getWaitingThread() {

        Thread active = new Thread() {
            Wait one = Wait.getInstance();

            public void run() {
                try {
                    one.waitForSeaWeather();
                } catch (InterruptedException ix) {
                }
            }
        };
        Thread blocked = new
                Thread() {
                    Wait one = Wait.getInstance();

                    public void run() {
                        try {
                            one.waitForSeaWeather();
                        } catch (InterruptedException ix) {
                        }
                    }
                };
        active.start();
        try {
            Thread.currentThread().sleep(100);
        } catch (InterruptedException ex) {
        }
        blocked.start();
        try {
            Thread.currentThread().sleep(100);
        } catch (InterruptedException ex) {
        }
        Thread.currentThread().yield();
        return blocked;
    }

    public static Thread getTimedWaitingThread() {
        Thread active = new Thread() {
            Wait one = Wait.getInstance();

            public void run() {
                try {
                    one.waitForTheDawn();
                } catch (InterruptedException ix) {
                }
            }
        };
        Thread blocked = new
                Thread() {
                    Wait one = Wait.getInstance();

                    public void run() {
                        try {
                            one.waitForTheDawn();
                        } catch (InterruptedException ix) {
                        }
                    }
                };
        active.start();
        try {
            Thread.currentThread().sleep(100);
        } catch (InterruptedException ex) {
        }
        blocked.start();
        try {
            Thread.currentThread().sleep(100);
        } catch (InterruptedException ex) {
        }
        Thread.currentThread().yield();
        return blocked;
    }

    public static Thread getTerminatedThread() {
        Thread active = new Thread() {
            public void run() {
                for (int i = 0; i < 5; i++) {
                }
            }
        };
        active.start();
        try {
            active.join();
        } catch (InterruptedException iex) {
        }
        return active;
    }

    public static synchronized void block() {
        while (3 == 3) {
        }
    }
}




