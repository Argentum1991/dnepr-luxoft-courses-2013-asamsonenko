package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class WalletIsBlockedException extends Exception {
private Long walletId;

    public WalletIsBlockedException(Long walletId,String message ) {
        super(message);
        this.walletId = walletId;
    }

    public Long getWalletId() {
        return walletId;
    }
}
