package com.luxoft.dnepr.courses.regular.unit3;


public class User implements UserInterface {
    Long id;
    String name;
    WalletInterface wallet;

    public User(Long id, String name, WalletInterface wallet) {
        this.id = id;
        this.name = name;
        this.wallet = wallet;
    }
    public User(){}
    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
       this.id=id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
       this.name=name;
    }

    @Override
    public WalletInterface getWallet() {
        return this.wallet;
    }

    @Override
    public void setWallet(WalletInterface wallet) {
       this.wallet=wallet;
    }
}
