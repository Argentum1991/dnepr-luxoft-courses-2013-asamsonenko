package com.luxoft.dnepr.courses.regular.unit6.dao;


import com.luxoft.dnepr.courses.regular.unit6.model.Employee;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

public class EmployeeDaoImpl<E extends Entity> extends AbstractDao {
    public EmployeeDaoImpl() {
        super(Employee.class);

    }
}
