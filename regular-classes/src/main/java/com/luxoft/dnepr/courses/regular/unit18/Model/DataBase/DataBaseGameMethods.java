package com.luxoft.dnepr.courses.regular.unit18.Model.DataBase;

public interface DataBaseGameMethods {
    public String getWord(int index);
    public int getBaseSize();
}
