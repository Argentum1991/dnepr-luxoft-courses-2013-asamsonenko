package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Calendar;
import java.util.Date;

/**
 * Product factory.
 * Simplifies creation of different kinds of products.
 */
public   class ProductFactory {

    public Bread createBread(String code, String name, double price, double weight) {
        return new Bread( code,  name, price,  weight);
    }

    public Beverage createBeverage(String code, String name, double price, boolean nonAlcoholic) {
        return new Beverage( code, name, price,  nonAlcoholic);
    }

    public Book createBook(String code, String name, double price, Date publicationDate) {
        return new Book(code, name, price, publicationDate);
    }
}
