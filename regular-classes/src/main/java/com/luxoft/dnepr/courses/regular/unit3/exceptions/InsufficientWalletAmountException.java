package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class InsufficientWalletAmountException extends Exception {
 private Long walletID;
 private BigDecimal amountToWithDraw;
 private BigDecimal amountInWallet;

    public InsufficientWalletAmountException(Long walletID, BigDecimal amountToWithDraw,
                                             BigDecimal amountInWallet,String message) {
        super(message);
        this.walletID = walletID;
        this.amountToWithDraw = amountToWithDraw;
        this.amountInWallet = amountInWallet;
    }

    public Long getWalletID() {
        return walletID;
    }

    public BigDecimal getAmountToWithDraw() {
        return amountToWithDraw;
    }

    public BigDecimal getAmountInWallet() {
        return amountInWallet;
    }
}
