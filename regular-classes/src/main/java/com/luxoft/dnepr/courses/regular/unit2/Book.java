package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct implements Product {
   Date publicationDate;
     public Book(String code, String name, double price, Date publicationDate){
     this.code=code;
     this.name=name;
     this.price=price;
     this.publicationDate=publicationDate;
     }
    public Date getPublicationDate() {
        return publicationDate;
    }

    @Override
    public boolean equals(Object o) {

        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        if (publicationDate.equals(book.publicationDate)) return true;

        return false;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + publicationDate.hashCode();
        return result;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Book book=(Book) super.clone();
        Date cloneData=(Date) publicationDate.clone();
        book.setPublicationDate(cloneData);
        return book;
    }
}
