package com.luxoft.dnepr.courses.regular.unit5.storage;


import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;


public class EntityStorage<E extends Entity> {
    private static EntityStorage instance;

    private final static Map<Long, Entity> entities = new HashMap();

    public static EntityStorage getInstance() {
        if (instance == null) {
            instance = new EntityStorage();
        }
        return instance;
    }

    public Map<Long, Entity> getEntities() {
        return entities;
    }

}
