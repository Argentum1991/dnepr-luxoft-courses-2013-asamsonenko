package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Collections;
import java.util.Map;

public class AbstractDao<E extends Entity> implements IDao<E> {
    protected static Long maxId;
    private Class<E> type;

    public AbstractDao(Class<E> type) {
        this.type = type;
        if (maxId == null) {
            maxId = new Long(0);
        }
    }

    public E create() {
        try {
            return type.getDeclaredConstructor().newInstance();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public E save(E entity) throws UserAlreadyExist {
        Map<Long, Entity> tmpMap = EntityStorage.getInstance().getEntities();

        if (null == entity.getId()) {
            maxId = new Long(maxId + 1);
            entity.setId(maxId);
            tmpMap.put(maxId, entity);
            return entity;
        }
        Long tmpId = entity.getId();
        if (tmpMap.containsKey(entity.getId())) {
            throw new UserAlreadyExist();
        } else {
            if (tmpId.compareTo(maxId) > 0) {
                maxId = tmpId;
            }
            tmpMap.put(entity.getId(), entity);
        }
        return entity;
    }

    @Override
    public E update(E entity) throws UserNotFound {
        Map<Long, Entity> tmpMap = EntityStorage.getInstance().getEntities();
        if (null == entity.getId() || !tmpMap.containsKey(entity.getId())) {
            throw new UserNotFound();
        }
        // @SuppressWarnings("unchecked") E oldValue = (E) tmpMap.get(entity.getId());
        tmpMap.put(entity.getId(), entity);
        //return oldValue;
        return entity;
    }

    @Override
    public E get(long id) {
        Map<Long, Entity> tmpMap = EntityStorage.getInstance().getEntities();
        if (!tmpMap.containsKey(id)) {
            return null;
        }
        @SuppressWarnings("uncheked") E result = (E) tmpMap.get(id);
        return result;
    }

    @Override
    public boolean delete(long id) {
        Map<Long, Entity> tmpMap = EntityStorage.getInstance().getEntities();
        if (!tmpMap.containsKey(id)) {
            return false;
        }
        if (maxId.compareTo(id) == 0) {
            maxId = Collections.max(tmpMap.keySet());
        }
        tmpMap.remove(id);
        return true;
    }
}
