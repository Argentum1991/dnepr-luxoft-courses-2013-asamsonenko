package com.luxoft.dnepr.courses.regular.unit4;


import java.util.*;


public class EqualSet<E> extends AbstractSet<E> {
    private E[] elementData;
    private int actualSize = 0;
    private double loadFactor=0.75;
    Iterator<E> myIterator=null;
    private int bruttoSize=0;

    public EqualSet() {
        this(10);
    }

    public EqualSet(Collection<? extends E> collection) {
        this(10);
        this.addAll(collection);
    }

    @SuppressWarnings("unchecked")
    public EqualSet(int initialCapacity) {
        super();
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal Capacity: " +
                    initialCapacity);
        this.elementData = (E[]) new Object[initialCapacity];

        myIterator = new MyIterator<E>();


    }
    public int getBruttoSize() {
        bruttoSize=elementData.length;
        return bruttoSize;
    }
    @Override
    public Iterator<E> iterator() {
       return new MyIterator<E>();
    }

    @Override
    public int size() {
        return actualSize;
    }



    @Override
    public boolean add(E e) {
        int i = 0;
        Iterator<E> it = new MyIterator<E>();

        while (it.hasNext()) {
            E tmpElement = it.next();

            if (e == null) {
                if (tmpElement == null) {
                    it=null;
                    return false;
                }
            } else {
                if (tmpElement != null) {
                    if (tmpElement.equals(e)) {
                        return false;
                    }
                }
            }
        }

        ensureCapacity(actualSize + 1);
        elementData[actualSize - 1] = e;
        it = null;
        return true;
    }


    private void ensureCapacity(int desiredIndex) {
        if (desiredIndex > (loadFactor * elementData.length)) {
            @SuppressWarnings("unchecked") E[] tmpArray = (E[]) new Object[elementData.length * 2];
            System.arraycopy(elementData, 0, tmpArray, 0, actualSize);
            this.elementData = tmpArray;
        }
        actualSize += 1;
    }

    class MyIterator<E> implements Iterator<E> {
        int counter = -1;

        @Override
        public boolean hasNext() {
            if (counter + 1 < actualSize) {
                return true;
            }
            return false;
        }

        @Override
        public E next() throws NoSuchElementException {
            counter += 1;
            if (counter > actualSize) {
                throw new NoSuchElementException("attempt to adress to element with index over actual size of array");
            }
            @SuppressWarnings("unchecked") E returnElement = (E) elementData[counter];
            return returnElement;
        }

        @Override
        public void remove() throws NoSuchElementException {
           if (counter<-1){throw new IllegalStateException
                   ("attempt ot adress to element with index-1");}
            if (actualSize != 0 ) {
                System.arraycopy(elementData, counter + 1, elementData, counter, actualSize - (counter + 1));
                actualSize -= 1;
                counter-=1;

            } else {
                throw new  IllegalStateException("attempt to adress to element with index -1");
            }
        }
    }

}


