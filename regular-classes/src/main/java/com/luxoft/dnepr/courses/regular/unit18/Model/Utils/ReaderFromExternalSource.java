package com.luxoft.dnepr.courses.regular.unit18.Model.Utils;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReaderFromExternalSource implements Callable<Boolean> {
    String source;
    String pattern;
    Collection<String> destination;
    Pattern pat;


    public ReaderFromExternalSource(String source, Collection<String> destination) {
        this.source = source;
        this.destination = destination;
    }


    public ReaderFromExternalSource(String source, String pattern, Collection<String> destination) {
        this.source = source;
        this.pattern = pattern;
        this.destination = destination;
    }

    @Override
    public Boolean call() throws IOException {
        if (pattern != null) {
            pat = Pattern.compile(pattern);
        }
        try (Scanner scanner =
                     new Scanner(new BufferedReader(new FileReader(source)))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (pattern != null) {
                    findPattern(line);
                } else {
                    destination.add(line);
                }
            }
        } catch (IOException iex) {
            iex.printStackTrace();
        }
        return true;
    }

    void findPattern(String line) {
        Matcher matcher = pat.matcher(line);
        while (matcher.find()) {
            destination.add(matcher.group());
        }
    }
}
