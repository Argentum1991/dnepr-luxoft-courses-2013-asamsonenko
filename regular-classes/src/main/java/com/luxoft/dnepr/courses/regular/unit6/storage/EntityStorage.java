package com.luxoft.dnepr.courses.regular.unit6.storage;


import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;


public class EntityStorage<E extends Entity> {
    public static ConcurrentMap<Long, Entity> entities = null;
    private static EntityStorage instance;

    private EntityStorage() {
    }

    public static EntityStorage getInstance() {
        if (instance == null) {
            entities = new ConcurrentHashMap<>();
            instance = new EntityStorage();
        }
        return instance;
    }

    public synchronized <E extends Entity> E putInStorage(E entity) throws EntityAlreadyExistException {
        if (entity.getId() != null) {
            if (entities.containsKey(entity.getId())) {
                throw new EntityAlreadyExistException();
            }
            else {entities.putIfAbsent(entity.getId(), entity);}
        }
        Long id2Wright = getNextId();
        entity.setId(id2Wright);
        entities.putIfAbsent(id2Wright, entity);

        return entity;
    }

    public synchronized boolean remove(Long id) {
        return entities.remove(id, entities.get(id));
    }

    public Long getNextId() {
        Iterator<Long> itr=entities.keySet().iterator();
        long maxId=0;
        while (itr.hasNext()){
        long curId=  itr.next();
        if (maxId<curId){maxId=curId;}
        }
        return maxId + 1;
    }

    public synchronized E updateInStorage(E entity) throws EntityNotFoundException {
    if (entity!=null){
        if (entities.containsKey(entity.getId())) {
            entities.put(entity.getId(), entity);
        } else {
            throw new EntityNotFoundException();
        }
    }
        return entity;
    }

    public E getElement(Long id) {
        @SuppressWarnings("checked") E returnEntity = (E) entities.get(id);
        return returnEntity;
    }

    public long returnSizeOfStorage() {
        return entities.keySet().size();
    }
}



