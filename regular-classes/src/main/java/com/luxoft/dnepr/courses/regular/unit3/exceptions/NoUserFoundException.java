package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class NoUserFoundException extends Exception {
 private Long userId;

    public NoUserFoundException(Long userId,String message ) {
        super(message);
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }
}
