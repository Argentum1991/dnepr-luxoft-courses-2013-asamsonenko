package com.luxoft.dnepr.courses.regular.unit18.Model.Constants;


public interface Commands {
    // User's commands
    String YES = "YES";
    String NO = "NO";
    String EXIT = "EXIT";
    String HELP = "HELP";
    String ABOUT = "ABOUT";
    String RESULT = "RESULT";
}
