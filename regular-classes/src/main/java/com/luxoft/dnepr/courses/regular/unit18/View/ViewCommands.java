package com.luxoft.dnepr.courses.regular.unit18.View;


public interface ViewCommands {
    public void placeData(String data);

    public String getUserAnswers();

}
