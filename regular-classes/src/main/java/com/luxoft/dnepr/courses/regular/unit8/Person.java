package com.luxoft.dnepr.courses.regular.unit8;


import java.io.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Person implements Externalizable {
    private String name;
    private Gender gender;
    private String ethnicity;
    private Date birthDate;
    private Person father;
    private Person mother;


    public Person(String name, Gender gender, String ethnicity,
                  Date birthDate, Person father, Person mother) {
        this.name = name;
        this.gender = gender;
        this.ethnicity = ethnicity;
        this.birthDate = birthDate;
        this.father = father;
        this.mother = mother;
    }

    public Person() {

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        DateFormat dateFormatter;
        dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault());
        String dateForMother = "";
        String dateForFather = "";
     //   System.out.println(in);
        String forParser = in.readUTF();
       // System.out.println("Person:" + forParser);

        List<PosTag> listTag = countTag(forParser);
        Pattern patName = Pattern.compile("name.:.(\\b([\\w\\-_'])+\\b)");
        Pattern patGender = Pattern.compile("gender.:.(\\b([\\w\\-_'])+\\b)");
        Pattern patEthnicity = Pattern.compile("ethnicity.:.(\\b([\\w\\-_'])+\\b)");
        Pattern patBirthDate = Pattern.compile("birthdate.:.(.*?)[,\\}]");
        Pattern patParent = Pattern.compile("(mother.:.$)|(father.:.$)");
        Matcher one = null;

        String tmpRes = null;
        one = patName.matcher(forParser);
        while (one.find()) {
            tmpRes = one.group(1);
            if (checkForCondition(one.start(), one.end(), listTag)) {
                setName(tmpRes);
                break;
            }
        }
        one = patEthnicity.matcher(forParser);
        while (one.find()) {
            tmpRes = one.group(1);
            if (checkForCondition(one.start(), one.end(), listTag)) {
                setEthnicity(tmpRes);
                break;
            }
        }
        one = patBirthDate.matcher(forParser);
        while (one.find()) {
            tmpRes = one.group(1);
            if (checkForCondition(one.start(), one.end(), listTag)) {
                try {
                    setBirthDate(dateFormatter.parse(tmpRes));
                    break;
                } catch (ParseException pex) {
                    pex.printStackTrace();
                }
            }
        }
        one = patGender.matcher(forParser);
        while (one.find()) {
            tmpRes = one.group(1);
            if (checkForCondition(one.start(), one.end(), listTag)) {
                setGender(Gender.FEMALE);
                if (tmpRes.equals("MALE")) {
                    setGender(Gender.MALE);
                    break;
                }
            }
        }
        for (PosTag entry : listTag) {
            Matcher parent = patParent.matcher(forParser.substring(0, entry.startPos));
            if (parent.find()) {
                String found = parent.group();
                if (found.equals(parent.group(1)) && (entry.endPos - entry.startPos > 2)) {
                    if (entry.endPos - entry.startPos > 3) {
                        dateForMother = forParser.substring(entry.startPos + 1, entry.endPos);
                        this.mother = addParent(dateForMother);
                    }
                }
                if (found.equals(parent.group(2)) && (entry.endPos - entry.startPos > 2)) {
                    if (entry.endPos - entry.startPos > 3) {
                        dateForFather = forParser.substring(entry.startPos+1, entry.endPos);
                        this.father = addParent(dateForFather);
                    }
                }
            }
        }
      //  System.out.println(getName());
        //System.out.println(getMother());



        //System.out.println(getFather());

    }

    private boolean checkForCondition(int startPos, int endPos, List<PosTag> list) {
        if (!(startPos > list.get(0).startPos && endPos < list.get(0).endPos)) {
            if (!(startPos > list.get(1).startPos && endPos < list.get(1).endPos)) {
                return true;
            }
        }
        return false;
    }

    private Person addParent(String entry) throws ClassNotFoundException {
        Person parent = new Person();
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeUTF(entry);
            oos.flush();
            baos.flush();
            oos.close();

            byte[] byteToArray = baos.toByteArray();
            ByteArrayInputStream bais = new ByteArrayInputStream(byteToArray);
            ObjectInputStream ois = new ObjectInputStream(bais);
            parent.readExternal(ois);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
        return parent;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(this.toString());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Date getBirthDate() {
        DateFormat dateFormatter;
        dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault());
        String dateOut = dateFormatter.format(this.birthDate);
        Date dateReturn = null;
        try {
            dateReturn = dateFormatter.parse(dateOut);
        } catch (ParseException pex) {
            pex.printStackTrace();
        }
        return dateReturn;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    @Override
    public String toString() {
        DateFormat dateFormatter;
        dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault());

        Map<String, String> property = new HashMap<String, String>();
        property.put("name", getName());
        if (getBirthDate() != null) {
            property.put("birthdate", dateFormatter.format(getBirthDate()));
        } else {

            property.put("birthdate", null);
        }
        if (getGender() != null) {
            property.put("gender", getGender().toString());
        } else {
            property.put("gender", null);
        }

        property.put("ethnicity", getEthnicity());

        if (getFather() == null || getMother() == null) {
            property.put("father", null);
            property.put("mother", null);
        } else {
            property.put("father", getFather().toString());
            property.put("mother", getMother().toString());
        }
        Iterator itr = property.entrySet().iterator();
        Map.Entry<String, String> pairs;
        StringBuilder pairsForWriting = new StringBuilder("{");

        while (itr.hasNext()) {
            pairs = (Map.Entry<String, String>) itr.next();
            if ((pairs.getKey().equals("father") || pairs.getKey().equals("mother"))
                    && pairs.getValue() == null) {
                pairsForWriting.append(formatKeyValue(pairs.getKey(), "{}"));
            } else {
                pairsForWriting.append(formatKeyValue(pairs.getKey(), pairs.getValue()));
            }

        }
        pairsForWriting.deleteCharAt(pairsForWriting.length() - 1);
        pairsForWriting.append("}");
        return pairsForWriting.toString();
    }
//father":{"name":"Vasiliy","gender":"MALE","ethnicity":"ukrainian","birthDate":"May 21, 2013 3:31:33 PM"}

    public String formatKeyValue(String key, String value) {
        return String.format("\"%s\":\"%s\",", key, value);
    }

    public List<PosTag> countTag(String pattern) {
        //openedTag is { or [ and closedTag is } or ]  accordingly
        CharSequence openTagPerson = String.valueOf('{');
        CharSequence closeTagPerson = String.valueOf('}');
        int countOpenPersonTag = 0;
        int countClosePersonTag = 0;
        CharSequence curChar = "a";
        boolean checkPerson = false;
        int startPosPerson = 0;
        int endPosPerson = 0;

        List<PosTag> listForPerson = new ArrayList<>();

        for (int i = 0; i < pattern.length(); i++) {
            curChar = String.valueOf(pattern.charAt(i));
            if (curChar.equals(openTagPerson)) {
                countOpenPersonTag++;
                if (!checkPerson) {
                    checkPerson = true;
                    startPosPerson = i;
                }
            }
            if (curChar.equals(closeTagPerson)) {
                countClosePersonTag++;
            }
            if (checkPerson && countClosePersonTag == countOpenPersonTag) {
                endPosPerson = i;
                listForPerson.add(new PosTag(startPosPerson, endPosPerson));
                checkPerson = false;
            }
        }
        return listForPerson;
    }

    class PosTag implements Comparable<PosTag> {
        int startPos;
        int endPos;

        PosTag(int startPos, int endPos) {
            this.startPos = startPos;
            this.endPos = endPos;
        }

        public int compareTo(PosTag pairEndStart) {
            return this.startPos - pairEndStart.startPos;
        }

        public String toString() {
            return ("START=" + this.startPos + " / END=" + this.endPos);
        }
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

       // if (birthDate != null ? !birthDate.equals(person.birthDate) : person.birthDate != null) return false;
        if (ethnicity != null ? !ethnicity.equals(person.ethnicity) : person.ethnicity != null) return false;
        if (gender != person.gender) return false;
        if (name != null ? !name.equals(person.name) : person.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (ethnicity != null ? ethnicity.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        return result;
    }
}
