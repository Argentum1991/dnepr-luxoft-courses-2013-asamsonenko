package com.luxoft.dnepr.courses.regular.unit6.exception;


public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException() {
        super();
    }
}
