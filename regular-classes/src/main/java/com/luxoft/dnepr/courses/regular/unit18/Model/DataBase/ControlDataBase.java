package com.luxoft.dnepr.courses.regular.unit18.Model.DataBase;

import com.luxoft.dnepr.courses.regular.unit18.Model.Utils.ReaderFromExternalSource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class ControlDataBase implements DataBaseGameMethods, DataBaseControllerMethods {
    // group0 are words, which length is greater than four symbols
    static final String wholeWord = "\\b([\\w\\-_']){4,}\\b";
    Set<String> wordBase = new HashSet<>();
    ExecutorService threadPoolExecutor;
    List<Future<Boolean>> tasks;
    List<String> baseForGame = new ArrayList<>();

    public ControlDataBase(ExecutorService threadPoolExecutor, List<Future<Boolean>> tasks) {
        this.tasks = tasks;
        this.threadPoolExecutor = threadPoolExecutor;
    }

    public void addNewData(String[] sources) {

        for (String sourceURI : sources) {
            tasks.add(threadPoolExecutor.submit(
                    new ReaderFromExternalSource(sourceURI, wholeWord, wordBase)));
        }
    }

    @Override
    public String isPrepareForGame(int elementIndex) {
        baseForGame.addAll(wordBase);
        String checkWord = baseForGame.get(elementIndex);
        return checkWord;
    }

    public String getWord(int index) {
        return baseForGame.get(index);
    }

    public int getBaseSize() {
        return baseForGame.size();
    }
}


