package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class LimitExceededException extends Exception {
    private Long walletId;
    private BigDecimal amountToTransfer;
    private BigDecimal amountInWallet;

    public LimitExceededException(long walletId, BigDecimal amountToTransfer,
                                  BigDecimal amountInWallet, String message) {
        super(message);
        this.walletId = walletId;
        this.amountToTransfer = amountToTransfer;
        this.amountInWallet = amountInWallet;

    }


    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getAmountToTransfer() {
        return amountToTransfer;
    }

    public BigDecimal getAmountInWallet() {
        return amountInWallet;
    }
}
