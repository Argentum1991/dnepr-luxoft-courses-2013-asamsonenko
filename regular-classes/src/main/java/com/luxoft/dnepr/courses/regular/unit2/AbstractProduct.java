package com.luxoft.dnepr.courses.regular.unit2;

public abstract class AbstractProduct implements Product, Cloneable {
  String code;
  String name;
  double  price;

    @Override
    public boolean equals(Object o) {
         if (o == null || getClass() != o.getClass()) return false;
         AbstractProduct that = (AbstractProduct) o;
        if (code.equals(that.code)&& name.equals(that.name)){return true;}
        return false;
    }

    @Override
    public int hashCode() {
        int result = code.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getCode() {
    return  code;
    }

    public String getName() {
    return name;
    }

    public double getPrice(){
    return price;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
       this.price = price;
    }
}
