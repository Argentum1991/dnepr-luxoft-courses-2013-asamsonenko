package com.luxoft.dnepr.courses.regular.unit6.model;


public class Entity {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
