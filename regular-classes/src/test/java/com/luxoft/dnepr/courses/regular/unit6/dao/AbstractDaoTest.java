package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Employee;
import com.luxoft.dnepr.courses.regular.unit6.model.Redis;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AbstractDaoTest {
    public static IDao<Employee> EmpFactory = new EmployeeDaoImpl<Employee>();
    public static IDao<Redis> RedisFactory = new RedisDaoImpl<Redis>();

    @Test

    public void testSave() throws Exception {
        for (int i = 0; i < 50; i++) {
            new Thread(new SaveTask()).start();
        }
        Thread.sleep(5500);
        Assert.assertEquals(2500, EntityStorage.getInstance().returnSizeOfStorage());
        testUpdateAndDelete();
    }

    public void testUpdateAndDelete() throws Exception {
        Thread tRemove1 = new Thread() {
            public void run() {
                for (int i = 0; i < 500; i++) {
                    EmpFactory.delete(i);
                }
            }
        };

        Thread tRemove2 = new Thread() {
            public void run() {
                for (int i = 500; i <= 1000; i++) {
                    EmpFactory.delete(i);
                }
            }
        };
        Thread tUpdate = new
                Thread() {
                    public void run() {
                        for (int i = 0; i <= 1000; i++) {
                           try{
                            EmpFactory.update(EmpFactory.get(i));
                           }
                           catch (EntityNotFoundException ex){}

                        }
                    }
                };

        tRemove1.start();

        tUpdate.start();

        tRemove2.start();
        Thread.sleep(2500);
        List ol=new ArrayList(EntityStorage.entities.keySet());
        Collections.sort(ol);
        System.out.print(ol);
        Assert.assertEquals(1500, EntityStorage.getInstance().returnSizeOfStorage());
    }


    class SaveTask implements Runnable {
        public void run() {
            for (int i = 0; i < 50; i++) {
                Employee emp = EmpFactory.create();
                EmpFactory.save(emp);
            }
        }
    }

}



