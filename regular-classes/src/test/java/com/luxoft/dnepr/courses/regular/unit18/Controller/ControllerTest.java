package com.luxoft.dnepr.courses.regular.unit18.Controller;


import com.luxoft.dnepr.courses.regular.unit18.View.GUIMakerStub;
import org.junit.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ControllerTest {
    GUIMakerStub guiMaker = new GUIMakerStub();
    @Test
    public void testForTest() throws Exception {
        ExecutorService threadPool= Executors.newCachedThreadPool();
        Future<Boolean> future= threadPool.submit(new Launcher());
        future.get();
        for (String entry:guiMaker.getInput()){
          System.out.println(entry);
        }


    }
    class Launcher implements Callable<Boolean>{
        @Override
        public Boolean call() throws Exception {
            Controller.startFromTest(guiMaker);
            return true ;
        }
    }
}
