package com.luxoft.dnepr.courses.regular.unit3;

import org.junit.Before;


import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Test2 {

    private static final String CURRENT_JAVA_VERSION = System.getProperty("java.version");
    private Bank bank;

    @Before
    public void initBank() {
        bank = new Bank(CURRENT_JAVA_VERSION);
        Map<Long, UserInterface> users = new HashMap();
        users.put(1L, createUser(1L, "aaa", 1L, new BigDecimal(100), new BigDecimal(1000), WalletStatus.ACTIVE));
        users.put(2L, createUser(2L, "bbb", 2L, new BigDecimal(100), new BigDecimal(500), WalletStatus.ACTIVE));
        users.put(3L, createUser(3L, "ccc", 3L, new BigDecimal(100), new BigDecimal(1000), WalletStatus.BLOCKED));
        users.put(4L, createUser(4L, "ddd", 4L, new BigDecimal(100), new BigDecimal(500), WalletStatus.BLOCKED));
        users.put(5L, createUser(5L, "eee", 5L, new BigDecimal(100), new BigDecimal(101), WalletStatus.ACTIVE));
        bank.setUsers(users);
    }

    @Test
    public void testNoUserFound() {
        try {
            bank.makeMoneyTransaction(1L, 10L, new BigDecimal(300));
            Assert.fail();
        } catch (NoUserFoundException e) {
            Assert.assertEquals(Long.valueOf(10), e.getUserId());
        } catch (TransactionException e) {
            Assert.fail();
        }
    }

    @Test
    public void testSenderWalletIsBlocked() {
        try {
            bank.makeMoneyTransaction(3L, 1L, new BigDecimal(100));
            Assert.fail();
        } catch (NoUserFoundException e) {
            Assert.fail("Users 1 and 3 exist");
        } catch (TransactionException e) {
            Assert.assertEquals("User 'ccc' wallet is blocked", e.getMessage());
        }

        try {
            bank.makeMoneyTransaction(3L, 1L, new BigDecimal(10000));
            Assert.fail();
        } catch (NoUserFoundException e) {
            Assert.fail("Users 1 and 3 exist");
        } catch (TransactionException e) {
            Assert.assertEquals("User 'ccc' wallet is blocked", e.getMessage());
        }
    }

    @Test
    public void testRecipientWalletIsBlocked() {
        try {
            bank.makeMoneyTransaction(1L, 3L, new BigDecimal(100));
            Assert.fail();
        } catch (NoUserFoundException e) {
            Assert.fail("Users 1 and 3 exist");
        } catch (TransactionException e) {
            Assert.assertEquals("User 'ccc' wallet is blocked", e.getMessage());
        }

        try {
            bank.makeMoneyTransaction(1L, 3L, new BigDecimal(10000));
            Assert.fail();
        } catch (NoUserFoundException e) {
            Assert.fail("Users 1 and 3 exist");
        } catch (TransactionException e) {
            Assert.assertEquals("User 'ccc' wallet is blocked", e.getMessage());
        }
    }

    @Test
    public void testTransferWithInsufficientWalletAmount() {
        try {
            bank.makeMoneyTransaction(1L, 2L, new BigDecimal(300));
            Assert.fail();
        } catch (NoUserFoundException e) {
            Assert.fail("Users 1 and 2 exist");
        } catch (TransactionException e) {
            Assert.assertEquals("User 'aaa' has insufficient funds (100.00 < 300.00)", e.getMessage());
        }
    }

    @Test
    public void testTransferWithLimitExceeded() {
        try {
            bank.makeMoneyTransaction(1L, 5L, new BigDecimal(50));
            Assert.fail();
        } catch (NoUserFoundException e) {
            Assert.fail("Users 1 and 2 exist");
        } catch (TransactionException e) {
            Assert.assertEquals("User 'eee' wallet limit exceeded (100.00 + 50.00 > 101.00)", e.getMessage());
        }
    }

    private UserInterface createUser(Long userId, String name
            , Long walletId, BigDecimal amount, BigDecimal maxAmount, WalletStatus status) {
        WalletInterface wallet = new Wallet();
        wallet.setId(walletId);
        wallet.setAmount(amount);
        wallet.setMaxAmount(maxAmount);
        wallet.setStatus(status);
        UserInterface user = new User();
        user.setId(userId);
        user.setName(name);
        user.setWallet(wallet);
        return user;
    }
  // public User(Long id, String name, WalletInterface wallet)
}
