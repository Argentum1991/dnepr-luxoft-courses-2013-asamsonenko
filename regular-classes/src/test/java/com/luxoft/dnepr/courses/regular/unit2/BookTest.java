package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;


import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();
        book.setPublicationDate(new GregorianCalendar(2002,2,1).getTime());
        System.out.println();
       assertTrue(!book.getPublicationDate().equals(cloned.getPublicationDate()));
        Bread bread= productFactory.createBread("white","White",12,222);
        Bread bread2=(Bread) bread.clone();
        assertFalse(bread==bread2);
    }

    @Test
    public void testEquals() throws Exception {
        Beverage fanta=productFactory.createBeverage("fanta","Fanta",10,true);
        Beverage McFanta=productFactory.createBeverage("fanta","Fanta",22,true);
        assertTrue(fanta.equals(McFanta));
        Book book1=productFactory.createBook("AyneRand","Fountainhead",100,new GregorianCalendar(1926,0,1).getTime());
        Book book3=productFactory.createBook("AyneRand","Fountainhead",332,new GregorianCalendar(1926,0,1).getTime());
        assertTrue(book1.equals(book3));
    }
}
