package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class BankTest {
    UserInterface user1;
    UserInterface user2;
    UserInterface user3;
    UserInterface user4;
    Bank PB;
    Map<Long, UserInterface> users;

    @Before
    public void prepare() throws Exception {
        user1 = new User(WalletTest.id1, "Arkadiy", WalletTest.walletLowAmount);
        user2 = new User(WalletTest.id2, "Roman", WalletTest.walletLowMaxAmount);
        user3 = new User(WalletTest.id3, "Panfil", WalletTest.walletBlocked);
        user4 = new User(new Long(104), "Rustam", WalletTest.walletBlocked);

        users = new HashMap<Long, UserInterface>();
        users.put(user1.getId(), user1);
        users.put(user2.getId(), user2);
        users.put(user3.getId(), user3);
        users.put(user4.getId(), user4);
        PB = new Bank();
        PB.setUsers(users);
    }

    @Test
    public void test1MakeMoneyTransaction() throws Exception {
        //makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount)


        try {
            PB.makeMoneyTransaction(user4.getId(), user3.getId(), WalletTest.amount2);
            fail("Wallet of user who sends money is blocked");
        } catch (TransactionException tex) {
            String except = String.format("User '%s' wallet is blocked", "Rustam");
            assertEquals(except, tex.getMessage());
        }
        try {
            PB.makeMoneyTransaction(user2.getId(), user3.getId(), WalletTest.amount1);
            fail("Wallet of user who receives money is blocked");
        } catch (TransactionException tex) {
            String except = String.format("User '%s' wallet is blocked", "Panfil");
            assertEquals(except, tex.getMessage());
        }

        try {
            PB.makeMoneyTransaction(new Long(1833), user4.getId(), WalletTest.amount2);
            fail("User with id 1833 is absent in database");
        } catch (NoUserFoundException nox) {
            assertEquals(nox.getUserId(), new Long(1833));
        }
        try {
            PB.makeMoneyTransaction(user3.getId(), new Long(1011), WalletTest.amount2);
            fail("User with id 1011 is absent in database");
        } catch (NoUserFoundException nox) {
            assertEquals(nox.getUserId(), new Long(1011));
        }
    }

    @Test
    public void test2MakeMoneyTransaction() throws Exception {
        try {
            PB.makeMoneyTransaction(user1.getId(), user2.getId(), WalletTest.maxAmount);
            fail("User who sends money hasn't enough money");
        } catch (TransactionException tex) {
            String userName = "Arkadiy";
            BigDecimal curBalance = user1.getWallet().getAmount();
            String message = String.format("User '%s' has insufficient funds " +
                    "(%.2f < %.2f)", userName, curBalance, WalletTest.maxAmount);
            assertEquals(message, tex.getMessage());
        }
    }

    @Test
    public void test3MakeMoneyTransaction() throws Exception {
        //String message = String.format("User '%s' wallet limit exceeded
        // (%.2f + %.2f > %.2f)", userName, userWalletAmount, amount, maxAmount);
        try {
            PB.makeMoneyTransaction(user1.getId(), user2.getId(), new BigDecimal(32.01));
            fail("User who receives money is going to overflow limit");
        } catch (TransactionException tex) {
            String userName = "Roman";
            BigDecimal curBalance = user2.getWallet().getAmount();
            BigDecimal maxAmount = user2.getWallet().getMaxAmount();
            String message = String.format("User '%s' wallet limit " +
                    "exceeded(%.2f + %.2f > %.2f)", userName,
                    curBalance, new BigDecimal(32.01), maxAmount);
            assertEquals(message, tex.getMessage());
        }

    }

    @Test
    public void testExpectedIllegalJavaVersionErrorJ() throws Error {
        String JREversion = "1.8.0";
        try {
            Bank bank = new Bank(JREversion);
        } catch (IllegalJavaVersionError ijve) {
            String expectedJavaVersion = ijve.getExpectedJavaVersion();
            String actualJavaVersion = ijve.getActualJavaVersion();
            String toMsg = String.format("Sorry, but actual version JRE %s" +
                    " does not match to the expected %s",
                    actualJavaVersion, expectedJavaVersion);
            System.out.println(toMsg);
        }

    }


}
