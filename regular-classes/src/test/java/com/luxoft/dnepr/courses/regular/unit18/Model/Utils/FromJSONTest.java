package com.luxoft.dnepr.courses.regular.unit18.Model.Utils;

import junit.framework.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class FromJSONTest {
    @Test
    public void testFromJSON() throws Exception {
    List<String> listConfig1=new ArrayList<>();
    String s="{\"sources\":[\"sonnets.txt\"]}";
    String config="c:/config_msg.txt";
    Scanner scanner =new Scanner(new File(config));
    while (scanner.hasNextLine()){
    listConfig1.add(scanner.nextLine());
    }

    List<String> listConfig2=new ArrayList<>();
    listConfig2.add(s);
        FromJSON fromJSON=new FromJSON();
    DataBaseSources dataBaseSources=fromJSON.fromListJSONConfig(listConfig2,DataBaseSources.class);
        Assert.assertEquals("sonnets.txt",dataBaseSources.sources[0]);
    Msg msgInfo=fromJSON.fromListJSONConfig(listConfig1,Msg.class);
        Assert.assertTrue(msgInfo.about.startsWith("Lingustic analizator v1"));
        Assert.assertTrue(msgInfo.talkToUser[0].startsWith("Try again"));
        Assert.assertTrue(msgInfo.help[3].startsWith("RESULT"));
    }
    class DataBaseSources {
        String[] sources;
    }
    class Msg {
        String about;
        String[] help;
        String explanation;
        String[] talkToUser;
        String result;

    }
}
