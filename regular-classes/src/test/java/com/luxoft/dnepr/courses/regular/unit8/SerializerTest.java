package com.luxoft.dnepr.courses.regular.unit8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;


public class SerializerTest {
    FamilyTree smiths=null;
    @Before
    public void prepare(){
        Person fatherMother = new Person("Alex", Gender.MALE, "scottish", new Date(7, 12, 1931), null, null);
        Person fatherFather = new Person("Scott", Gender.MALE, "english", new Date(8, 10, 1937), null, null);
        Person motherFather = new Person("Rebeka", Gender.FEMALE, "irish", new Date(5, 12, 1935), null, null);
        Person motherMother = new Person("Mildred", Gender.FEMALE, "irish", new Date(18, 8, 1938), null, null);
        Person father = new Person("Richard", Gender.MALE, "english", new Date(11, 11, 1953), fatherFather, motherFather);
        father.setFather(fatherFather);
        father.setMother(motherMother);

        Person mother = new Person("Eva", Gender.FEMALE, "irish", new Date(11, 10, 1951), fatherMother, motherMother);
        mother.setFather(motherFather);
        mother.setMother(motherMother);
        Person root = new Person("Alfred", Gender.MALE, "english", new Date(01, 11, 1976), father, mother);
        root.setFather(father);
        root.setMother(mother);
        smiths = new FamilyTree(root);
    }

    @Test
    public void testSerialize() throws Exception {
      File file = new File("Check.txt");
        Serializer.serialize(file, smiths);
    }

    @Test
    public void testDeserialize() throws Exception {
        File fileFrom = new File("Check.txt");
        FamilyTree restoreSmiths = Serializer.deserialize(fileFrom);
        System.out.println(smiths);
        System.out.println("----------------------------------");
        System.out.println( restoreSmiths);
        Assert.assertEquals(smiths, restoreSmiths);
    }
}
