package com.luxoft.dnepr.courses.regular.unit4;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import com.luxoft.dnepr.courses.regular.unit4.EqualSet;

import java.util.*;


public class EqualSetTest {
    EqualSet<Integer> integerSet1;
    List<Integer> list;
    @Before
    public void prepare() throws Exception{
        list = new ArrayList<Integer>();
        for (int i=0; i<12;i++){
        list.add(i);
        }
    }
    @Test
    public void test1Constructor() throws Exception {
    integerSet1=new EqualSet<Integer>(list);
    Assert.assertEquals(12,integerSet1.size());
    Assert.assertEquals(20,integerSet1.getBruttoSize());
    integerSet1.add(null);
    integerSet1.add(null);
    Assert.assertEquals(13,integerSet1.size());
    integerSet1.add(11);
    integerSet1.add(15);
    Assert.assertEquals(14,integerSet1.size());
    EqualSet<Object> objectSet=new EqualSet<Object>();
    Assert.assertEquals(10,objectSet.getBruttoSize());
    }

    @Test
    public void testIteratorNext() throws Exception {
    integerSet1=new EqualSet<Integer>(list);
        /// test iterator
        Iterator itr = integerSet1.iterator();
        int i = -1;
        while (itr.hasNext()) {
            i++;
            Assert.assertEquals(itr.next(), list.get(i));
        }

        for (int ii=integerSet1.size()-1;ii>=0;ii--){
        itr.remove();
        }
        Assert.assertEquals(0,integerSet1.size());
    }
    @Test
    public void testClear() throws Exception{
    integerSet1=new EqualSet<Integer>(list);
    integerSet1.clear();
    Assert.assertEquals(0,integerSet1.size());
    }

    @Test
    public void testAddAll() throws Exception{
    integerSet1=new EqualSet<Integer>();
    integerSet1.addAll(list);
    Assert.assertEquals(12,integerSet1.size());
    }
    @Test
    public void testContains()throws Exception{
    integerSet1=new EqualSet<Integer>();
    integerSet1.addAll(list);
    Assert.assertTrue(integerSet1.contains(2));
    Assert.assertFalse(integerSet1.contains(281));
    Assert.assertEquals(12,integerSet1.size());
    integerSet1.add(null);
    Assert.assertEquals(13,integerSet1.size());
    Assert.assertTrue(integerSet1.contains(null));
    Assert.assertTrue(integerSet1.contains(null));
    }
    @Test
    public void testRemove() throws Exception {
    integerSet1=new EqualSet<Integer>();
    integerSet1.addAll(list);
    Assert.assertTrue(integerSet1.remove(10));
    Assert.assertFalse(integerSet1.remove(10));
    }


    @Test
    public void testSize() throws Exception {

    }

    @Test
    public void testAdd() throws Exception {

    }
}
