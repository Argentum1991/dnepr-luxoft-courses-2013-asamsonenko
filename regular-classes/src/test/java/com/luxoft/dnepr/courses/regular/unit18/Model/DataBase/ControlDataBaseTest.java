package com.luxoft.dnepr.courses.regular.unit18.Model.DataBase;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class ControlDataBaseTest {
    List<Future<Boolean>> tasks;
    ExecutorService threadPool;
    ControlDataBase controlDataBase;

    @Before
    public void prepare() {
        threadPool = Executors.newCachedThreadPool();
        tasks = new ArrayList<>();
        controlDataBase =
                new ControlDataBase(threadPool, tasks);

    }

    @Test
    public void testDataBase() throws Exception {
        String[] sources = {"c:/sonnets.txt"};
        controlDataBase.addNewData(sources);
        waitForTermination(tasks);
        Assert.assertTrue(controlDataBase.isPrepareForGame(0) != null);
        System.out.println(controlDataBase.getBaseSize());

    }

    public void waitForTermination(List<Future<Boolean>> tasks) {
        try {
            for (Future<Boolean> future : tasks) {
                future.get();
            }
        } catch (ExecutionException | InterruptedException iex) {
            iex.printStackTrace();
        }
    }
}
