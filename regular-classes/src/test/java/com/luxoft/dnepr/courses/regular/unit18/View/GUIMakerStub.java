package com.luxoft.dnepr.courses.regular.unit18.View;


import com.luxoft.dnepr.courses.regular.unit18.Model.Constants.Commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GUIMakerStub implements ViewCommands {
    List<String> commands;
    List<String> input;
    Iterator<String> itr;

    public GUIMakerStub() {
        input = new ArrayList<>();
        commands = new ArrayList<>();
        commands.add(Commands.ABOUT);
        for (int i = 0; i < 5; i++) {
            commands.add(Commands.YES);
        }
        for (int i = 0; i < 3; i++) {
            commands.add(Commands.NO);
        }
        commands.add("Cheat");
        commands.add(Commands.RESULT);
        itr = commands.iterator();
    }

    @Override
    public void placeData(String data) {
        input.add(data);
    }

    @Override
    public String getUserAnswers() {
        if (itr.hasNext()) {
            return itr.next();
        } else
            return Commands.EXIT;
    }
    public List<String> getInput(){
        return input;
    }
}
