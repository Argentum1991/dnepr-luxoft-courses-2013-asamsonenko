package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public final class WalletTest {
    public static BigDecimal maxAmount = new BigDecimal(309.101);
    public static BigDecimal amount1 = new BigDecimal(99.91);
    public static BigDecimal amount2 = new BigDecimal(150);
    public static Long id1 = new Long(101);
    public static Long id2 = new Long(102);
    public static Long id3 = new Long(103);
    //Wallet(WalletStatus status, BigDecimal maxAmount, Long id, BigDecimal amount)
    public static Wallet walletBlocked = new Wallet(WalletStatus.BLOCKED, maxAmount, id1, amount1);
    public static Wallet walletLowMaxAmount = new Wallet(WalletStatus.ACTIVE, amount2, id2, amount2);
    public static Wallet walletLowAmount = new Wallet(WalletStatus.ACTIVE, maxAmount, id3, amount1);


    @Test
    public void testCheckWithdrawal() throws Exception {
        try {
            walletBlocked.checkWithdrawal(amount1);

            fail("wallet is BLOCKED");
        } catch (WalletIsBlockedException wax) {
            assertEquals(wax.getWalletId(), walletBlocked.getId());
        }

        try {
            walletLowAmount.checkWithdrawal(maxAmount);
            fail("Insufficient Wallet Amount");
        } catch (InsufficientWalletAmountException iex) {
            assertTrue(iex.getAmountInWallet().compareTo(maxAmount) < 0);
        }

        // if conditions are satisfied, then no exception will be thrown
        walletLowMaxAmount.checkWithdrawal(amount1);
    }

    @Test
    public void testCheckTransfer() throws Exception {

        try {
            walletLowMaxAmount.checkTransfer(amount1);
            fail("amount in wallet are greater than settled limit");
        } catch (LimitExceededException lex) {
            assertTrue(lex.getAmountInWallet().add(lex.getAmountToTransfer()).compareTo(walletLowMaxAmount.getMaxAmount()) > 0);

        }
    }

}
