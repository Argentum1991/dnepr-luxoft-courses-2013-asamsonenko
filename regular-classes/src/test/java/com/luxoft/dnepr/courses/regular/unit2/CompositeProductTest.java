package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CompositeProductTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testAdd() {
        CompositeProduct compositeProduct = new CompositeProduct();

        assertEquals(0, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(1, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(2, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("pepper", "Mr.Pepper", 100, true));
        assertEquals(3, compositeProduct.getAmount());
    }
    @Test
    public void testAddCollections(){
        CompositeProduct compositeBeverage1 = new CompositeProduct();
        compositeBeverage1.add(factory.createBeverage("baycal", "Baycal", 10, true));
        assertEquals(1, compositeBeverage1.getAmount());

        compositeBeverage1.add(factory.createBeverage("jaffa", "Jaffa", 14, true));
        assertEquals(2, compositeBeverage1.getAmount());

        CompositeProduct compositeBeverage2 = new CompositeProduct();
        compositeBeverage2.add(factory.createBeverage("milk", "Smakovenky", 10, true));
        assertEquals(1, compositeBeverage2.getAmount());

        compositeBeverage2.add(factory.createBeverage("orange", "OrangeJuice", 14, true));
        assertEquals(2, compositeBeverage2.getAmount());

        compositeBeverage1.add(compositeBeverage2);
        assertEquals(4, compositeBeverage1.getAmount());

    }

    @Test
    public void testGetPrice() {
        CompositeProduct compositeProduct = new CompositeProduct();

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(10, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals((10 + 10)* 0.95, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 20, true));
        assertEquals((10 + 10 + 20) * 0.9, compositeProduct.getPrice(), 0);

        compositeProduct.remove(factory.createBeverage("cola", "Coca-cola", 20, true));
        assertEquals((10 + 10 ) * 0.95, compositeProduct.getPrice(), 0);

        compositeProduct.remove(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(10, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("pepper", "Mr.Pepper", 100, true));
        assertEquals((10 + 100) , compositeProduct.getPrice(), 0);

    }
    @Test
    public void testGetCompositePrice() {
        CompositeProduct compositeBeverage1 = new CompositeProduct();

        compositeBeverage1.add(factory.createBeverage("cola", "Coca-cola", 1, true));
        assertEquals(1, compositeBeverage1.getPrice(), 0);

        compositeBeverage1.add(factory.createBeverage("pepper", "Mr.Pepper", 2, true));
        assertEquals((1 + 2) , compositeBeverage1.getPrice(), 0);

        CompositeProduct compositeBeverage2 = new CompositeProduct();
        compositeBeverage2.add(factory.createBeverage("milk", "Smakovenky", 3, true));
        assertEquals(3, compositeBeverage2.getPrice(),0);

        compositeBeverage2.add(factory.createBeverage("orange", "OrangeJuice", 4, true));
        assertEquals(3+4, compositeBeverage2.getPrice(),0);

        compositeBeverage1.add(compositeBeverage2);
        assertEquals(3+4+1+2, compositeBeverage1.getPrice(),0);

    }
}
